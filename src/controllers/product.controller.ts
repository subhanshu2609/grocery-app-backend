import { Request, Response } from "express";
import { productService } from "../services/entities/product.service";
import { ProductCategoryTransformer } from "../transformers/product-category.transformer";
import { ProductSubCategoryTransformer } from "../transformers/product-sub-category.transformer";
import { ProductTransformer } from "../transformers/product.transformer";
import { ProductCategoryNotFoundException } from "../exceptions/product/product-category-not-found.exception";
import { ProductSubCategoryNotFoundException } from "../exceptions/product/product-sub-category-not-found.exception";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import * as fs from "fs";
import { ImageUploadType } from "../enums/image-upload-type.enum";
import { ProductCreateDto } from "../dtos/product/product-create.dto";
import { ProductUpdateDto } from "../dtos/product/product-update.dto";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { ENV_BASE_URL, ENV_S3_BASE_URL } from "../util/secrets.util";
import { ProductIndexDto } from "../dtos/product/product-index.dto";
import toBoolean from "validator/lib/toBoolean";
import Ajv from "ajv";
import { Product } from "../models/product.model";
import { isBoolean } from "util";
import { dbService } from "../services/db.service";
import { s3Service } from "../services/factories/s3.service";

export class ProductController {

  static async listProductCategories(req: Request, res: Response) {
    const categories = await productService.listCategories();
    return res.json({
      data: await new ProductCategoryTransformer().transformList(categories)
    });
  }

  static async listProductSubCategories(req: Request, res: Response) {
    const categoryId    = +req.params.categoryId;
    const subCategories = await productService.listSubCategories(categoryId);
    return res.json({
      data: await new ProductSubCategoryTransformer().transformList(subCategories)
    });
  }

  static async listProducts(req: Request, res: Response) {
    const subCategoryId = +req.params.subCategoryId;
    const city_id       = +req.user.city_id;
    const products      = await productService.listProducts(subCategoryId, city_id);
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async allProducts(req: Request, res: Response) {
    const filters  = req.query as ProductIndexDto;
    const city_id  = +req.user.city_id;
    const products = await productService.allProducts(city_id, filters, true);
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async productForAdmin(req: Request, res: Response) {
    let products: Product[] = [];
    const filters           = req.query as { query?: string };
    if (filters.query == "") {
      products = [];
    } else if (!filters.query) {
      products = await Product.findAll({
        where: {
          city_id: req.employee.city_id
        },
        order: [["title", "asc"]]});
    } else {
      products = await Product.findAll({
        where: {
          ["title" as string]: {
            like: "%" + filters.query + "%"
          },
          city_id: req.employee.city_id
        }
      });
    }
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async productBySCForAdmin(req: Request, res: Response) {
    const products = await Product.findAll({
      where: {
        sub_category_id: +req.params.subCategoryId
      },
      order: [["title", "asc"]]
    });
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async createProductCategory(req: Request, res: Response) {
    const inputData = req.body as { title: string };
    const category  = await productService.addCategory(inputData.title);
    return res.json({
      data: await new ProductCategoryTransformer().transform(category)
    });
  }

  static async createProductSubCategory(req: Request, res: Response) {
    req.body.category_id = +req.body.category_id;
    const inputData      = req.body as { title: string, category_id: number, type?: ImageUploadType };
    const image          = req.file;
    const ajv            = new Ajv();
    const schema         = JSON.parse(fs.readFileSync("./schema/product/sub-category-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    let subCategory;
    let uploadUrl = "";
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    subCategory = await productService.addSubCategory(inputData.title, inputData.category_id, uploadUrl);
    return res.json({
      data: await new ProductSubCategoryTransformer().transform(subCategory)
    });
  }

  static async createProduct(req: Request, res: Response) {
    const inputData          = req.body as ProductCreateDto;
    req.body.sub_category_id = +req.body.sub_category_id;
    req.body.mrp             = +req.body.mrp;
    req.body.selling_price   = +req.body.selling_price;
    req.body.city_id         = +req.body.city_id;
    req.body.is_under_gst    = toBoolean(req.body.is_under_gst);
    if (req.body.gst_rate) {
      req.body.gst_rate = +req.body.gst_rate;
    }
    const image  = req.file;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/product/product-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    let uploadUrl = "";
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    const product = await productService.addProduct(inputData, uploadUrl);
    return res.json({
      data: await new ProductTransformer().transform(product)
    });
  }

  static async updateProductCategory(req: Request, res: Response) {
    const categoryId = +req.params.categoryId;
    const inputData  = req.body as { title: string };
    const category   = await productService.showCategory(categoryId);
    if (!category) {
      throw new ProductCategoryNotFoundException();
    }
    const updatedCategory = await productService.updateCategory(category, inputData.title);
    return res.json({
      data: await new ProductCategoryTransformer().transform(updatedCategory)
    });
  }

  static async updateProductSubCategory(req: Request, res: Response) {
    const subCategoryId  = +req.params.subCategoryId;
    req.body.category_id = +req.body.category_id;
    const inputData      = req.body as { title: string, type?: ImageUploadType };
    const subCategory    = await productService.showSubCategory(subCategoryId);
    if (!subCategory) {
      throw new ProductSubCategoryNotFoundException();
    }
    const image  = req.file;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/product/sub-category-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    let updatedSubCategory;
    let uploadUrl = subCategory.image_url;
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    updatedSubCategory = await productService.updateSubCategory(subCategory, inputData.title, uploadUrl);
    return res.json({
      data: await new ProductSubCategoryTransformer().transform(updatedSubCategory)
    });
  }

  static async updateProduct(req: Request, res: Response) {
    const productId = +req.params.productId;
    if (req.body.is_under_gst) {
      req.body.is_under_gst = toBoolean(req.body.is_under_gst);
    }
    if (req.body.gst_rate) {
      req.body.gst_rate = +req.body.gst_rate;
    }
    if (req.body.mrp) {
      req.body.mrp = +req.body.mrp;
    }
    if (req.body.selling_price) {
      req.body.selling_price = +req.body.selling_price;
    }
    if (req.body.city_id) {
      req.body.city_id = +req.body.city_id;
    }
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    if (req.body.in_stock) {
      req.body.in_stock = toBoolean(req.body.in_stock);
    }


    const inputData = req.body as ProductUpdateDto;
    const product   = await productService.showProduct(productId);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const image  = req.file;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/product/product-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    let updatedProduct;
    let uploadUrl = product.image_url;
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    updatedProduct = await productService.updateProduct(product, inputData, uploadUrl);
    return res.json({
      data: await new ProductTransformer().transform(updatedProduct)
    });
  }

  static async deleteProductCategory(req: Request, res: Response) {
    const categoryId = +req.params.categoryId;

    const category = await productService.showCategory(categoryId);
    if (!category) {
      throw new ProductCategoryNotFoundException();
    }
    await productService.deleteCategory(category);
    return res.json("Success");
  }

  static async deleteProductSubCategory(req: Request, res: Response) {
    const subCategoryId = +req.params.subCategoryId;

    const subCategory = await productService.showSubCategory(subCategoryId);
    if (!subCategory) {
      throw new ProductSubCategoryNotFoundException();
    }
    await productService.deleteSubCategory(subCategory);
    return res.json("Success");
  }

  static async deleteProduct(req: Request, res: Response) {
    const productId = +req.params.productId;

    const product = await productService.showProduct(productId);
    if (!product) {
      throw new ProductNotFoundException();
    }
    await productService.deleteProduct(product);
    return res.json("Success");
  }

}
