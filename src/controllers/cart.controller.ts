import { NextFunction, Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { CartCreateDto } from "../dtos/cart/cart-create.dto";
import { cartService } from "../services/entities/cart.service";
import { CartTransformer } from "../transformers/cart.transformer";
import { CartEmptyException } from "../exceptions/cart/cart-empty.exception";
import { CartItemNotFoundException } from "../exceptions/cart/cart-item-not-found.exception";
import { InsufficientQuantityException } from "../exceptions/cart/insufficient-quantity.exception";
import Ajv from "ajv";
import * as fs from "fs";
import { Cart } from "../models/cart.model";
import { ProductTransformer } from "../transformers/product.transformer";

export class CartController {

  static async addToCart(req: Request, res: Response) {
    req.body.product_id  = +req.body.product_id;
    req.body.no_of_units = +req.body.no_of_units;
    const inputData      = req.body as CartCreateDto;
    const ajv            = new Ajv();
    const schema         = JSON.parse(fs.readFileSync("./schema/cart/cart-create.schema.json").toString());
    const valid          = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }

    const cart = await cartService.listCart(req.user.id);
    let updatedCart: Cart;
    if (!cart) {
      throw new CartItemNotFoundException();
    }

    if (inputData.no_of_units < 0) {
      throw new InsufficientQuantityException();
    } else if (inputData.no_of_units === 0) {
      updatedCart = await cartService.removeProductFromCart(cart, inputData.product_id);
    } else {
      updatedCart = await cartService.updateProductOfCart(cart, inputData);
    }
    updatedCart = await cartService.resetCart(updatedCart);

    return res.json({
      data: await new CartTransformer().transform(updatedCart)
    });
  }

  static async listCart(req: Request, res: Response) {
    const cart     = await cartService.listCart(req.user.id);
    const products = await cartService.productsFromCart(cart, req.user.city_id);
    return res.json({
      data : await new CartTransformer().transform(cart),
      extra: await new ProductTransformer().transformList(products)
    });
  }

  static async emptyCart(req: Request, res: Response) {
    let cart = await cartService.listCart(req.user.id);
    if (!cart) {
      throw new CartEmptyException();
    }
    cart = await cartService.emptyCart(cart);
    return res.json({
      data: await new CartTransformer().transform(cart)
    });
  }

  static async countProductOfCart(req: Request, res: Response) {
    const count = await cartService.countQuantityOfProduct(req.user.id, +req.params.productId);
    return res.json(count);
  }

}
