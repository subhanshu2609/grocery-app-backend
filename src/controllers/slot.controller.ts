import { NextFunction, Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import * as fs from "fs";
import { AttachmentNotFoundException } from "../exceptions/root/attachment-not-found.exception";
import { Attachment } from "../models/attachment.model";
import { ENV_BASE_URL, ENV_S3_BASE_URL } from "../util/secrets.util";
import { Sequelize } from "sequelize";
import { s3Service } from "../services/factories/s3.service";
import { slotService } from "../services/entities/slot.service";
import { SlotCreateDto } from "../dtos/slot/slot-create.dto";
import Ajv from "ajv";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";

export class SlotController {

  static async viewSlots(req: Request, res: Response) {
    const slots = await slotService.listSlots();
    return res.json({
      data: slots
    });
  }

  static async viewAllSlots(req: Request, res: Response) {
    const slots = await slotService.listAllSlots();
    return res.json({
      data: slots
    });
  }

  static async createSlot(req: Request, res: Response) {
    const inputData = req.body as SlotCreateDto;
    console.log("hello");
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/slot/slot-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const slot = await slotService.createSlot(inputData);
    return res.json({
      data: slot
    });
  }

  static async deleteSlot(req: Request, res: Response) {
    const slot = await slotService.showSlot(+req.params.slotId);
    if (!slot) {
      throw new UnauthorizedException("Slot Not Found", 601);
    }
    await slotService.deleteSlot(slot);
    return res.json("success");
  }

}

