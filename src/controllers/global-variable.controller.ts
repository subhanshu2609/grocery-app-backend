import { Request, Response } from "express";
import { GlobalVariable } from "../models/global-variable.model";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";

export class GlobalVariableController {

  static async show(req: Request, res: Response) {
    const variables = await GlobalVariable.findAll();
    return res.json({
      data: variables
    });
  }

  static async showVersion(req: Request, res: Response) {
    const version = await GlobalVariable.findOne({
      where: {
        title: "Current Version"
      }
    });
    return res.json(version.value);
  }

  static async createVariable(req: Request, res: Response) {
    const inputData = req.body as { title: string; value: number };
    let variable    = null;
    try {
      variable = await GlobalVariable.create(inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    return res.json({
      data: variable
    });
  }

  static async updateVariables(req: Request, res: Response) {
    const value  = +req.body.value;
    let variable = null;
    try {
      variable = await GlobalVariable.update({value: value}, {where: {id: +req.params.variableId}});
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    return res.json({
      data: variable
    });
  }

  static async deleteVariable(req: Request, res: Response) {
    const variable = await GlobalVariable.findById(+req.params.variableId);
    if (!variable) {
      throw new Error("Variable Not Found");
    }
    return variable.destroy();
  }
}
