import { NextFunction, Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import Ajv from "ajv";
import * as fs from "fs";
import { Cart } from "../models/cart.model";
import { SubscriptionCreateDto } from "../dtos/subscription/subscription-create.dto";
import { productService } from "../services/entities/product.service";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import { subscriptionService } from "../services/entities/subscription.service";
import { SubscriptionTransformer } from "../transformers/subscription.transformer";
import { SubscriptionNotFoundException } from "../exceptions/cart/subscription-not-found.exception";
import { OrderTransformer } from "../transformers/order.transformer";
import { Subscription } from "../models/subscription.model";
import moment from "moment";

export class SubscriptionController {

  static async addToSubscription(req: Request, res: Response) {
    req.body.product_id  = +req.body.product_id;
    req.body.no_of_units = +req.body.no_of_units;
    const inputData      = req.body as SubscriptionCreateDto;
    const ajv            = new Ajv();
    const schema         = JSON.parse(fs.readFileSync("./schema/subscription/subscription-create.schema.json").toString());
    const valid          = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const product = await productService.showProductById(inputData.product_id);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const subscription = await subscriptionService.addToSubscription(inputData, product, req.user);
    return res.json({
      data: await new SubscriptionTransformer().transform(subscription)
    });
  }

  static async viewMySubscription(req: Request, res: Response) {
    const subscriptions = await subscriptionService.showByUser(req.user.id);
    return res.json({
      data: await new SubscriptionTransformer().transformList(subscriptions)
    });
  }

  static async cancelSubscription(req: Request, res: Response) {
    const subscription = await subscriptionService.show(+req.params.subscriptionId);
    await subscriptionService.delete(subscription);
    return res.json("success");
  }

  static async addToOrders(req: Request, res: Response) {
    const orders = await subscriptionService.addToOrder();
    return res.json({
      data: await new OrderTransformer().transformList(orders)
    });
  }

  static async viewCurrentSubscriptions(req: Request, res: Response) {
    const subscriptions = await Subscription.findAll({
      where   : {
        start_date: {
          $lte: moment().format("YYYY-MM-DD")
        },
        end_date  : {
          $gte: moment().format("YYYY-MM-DD")
        }
      }, order: [["id", "desc"]]
    });
    return res.json({
      data: await new SubscriptionTransformer().transformList(subscriptions)
    });
  }

  static async viewAllSubscriptions(req: Request, res: Response) {
    const subscriptions = await Subscription.findAll({order: [["id", "desc"]]});
    return res.json({
      data: await new SubscriptionTransformer().transformList(subscriptions)
    });
  }

}
