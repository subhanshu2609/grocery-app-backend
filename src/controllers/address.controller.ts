import { NextFunction, Request, Response } from "express";
import { addressService } from "../services/entities/address.service";
import { CityTransformer } from "../transformers/city.transformer";
import { LocationTransformer } from "../transformers/location.transformer";
import { AreaTransformer } from "../transformers/area.transformer";
import { AddressType } from "../enums/address-type.enum";
import { AddressCreateDto } from "../dtos/address/address-create.dto";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import * as fs from "fs";
import { Area } from "../models/address/area.model";
import { Location } from "../models/address/location.model";
import { AddressNotFoundException } from "../exceptions/address/address-not-found.exception";
import Ajv from "ajv";

export class AddressController {
  static async listCities(req: Request, res: Response) {
    const cities = await addressService.listCities();
    return res.json({
      data: await new CityTransformer().transformList(cities)
    });
  }

  static async listLocations(req: Request, res: Response) {
    const cityId    = +req.params.cityId;
    const locations = await addressService.listLocations(cityId);
    return res.json({
      data: await new LocationTransformer().transformList(locations)
    });
  }

  static async listAreas(req: Request, res: Response) {
    const locationId = +req.params.locationId;
    const areas      = await addressService.listAreas(locationId);
    return res.json({
      data: await new AreaTransformer().transformList(areas)
    });
  }

  static async addAddresses(req: Request, res: Response) {
    req.body.city_id     = +req.body.city_id;
    req.body.location_id = +req.body.location_id;
    const inputData      = req.body as AddressCreateDto;
    let image;
    const ajv            = new Ajv();
    let schema;
    let valid;
    switch (inputData.type) {
      case AddressType.CITY:
        schema = JSON.parse(fs.readFileSync("./schema/address/city-create.schema.json").toString());
        valid  = await ajv.validate(schema, inputData);
        if (!valid) {
          throw new UnprocessableEntityException(ajv.errors);
        }
        image = req.file;
        let city;
        if (image) {
          try {
            city = await addressService.addCity(inputData, image);
          } catch (e) {
            fs.unlinkSync(image.path.replace(/\\/g, "/"));
            throw e;
          }
        } else {
          city = await addressService.addCity(inputData);
        }
        return res.json({
          data: await new CityTransformer().transform(city)
        });


      case AddressType.LOCATION:
        schema = JSON.parse(fs.readFileSync("./schema/address/location-create.schema.json").toString());
        valid  = await ajv.validate(schema, inputData);
        if (!valid) {
          throw new UnprocessableEntityException(ajv.errors);
        }
        image = req.file;
        let location: Location;
        if (image) {
          try {
            location = await addressService.addLocation(inputData, image);
          } catch (e) {
            fs.unlinkSync(image.path.replace(/\\/g, "/"));
            throw e;
          }
        } else {
          location = await addressService.addLocation(inputData);
        }
        return res.json({
          data: await new LocationTransformer().transform(location)
        });


      case AddressType.AREA:
        schema = JSON.parse(fs.readFileSync("./schema/address/area-create.schema.json").toString());
        valid  = await ajv.validate(schema, inputData);
        if (!valid) {
          throw new UnprocessableEntityException(ajv.errors);
        }
        image = req.file;
        let area: Area;
        if (image) {
          try {
            area = await addressService.addArea(inputData, image);
          } catch (e) {
            fs.unlinkSync(image.path.replace(/\\/g, "/"));
            throw e;
          }
        } else {
          area = await addressService.addArea(inputData);
        }
        return res.json({
          data: await new AreaTransformer().transform(area)
        });
    }
  }

  static async updateImage(req: Request, res: Response) {
    const inputData = req.body as { type: AddressType };
    const addressId = +req.params.addressId;

    const address = await addressService.findAddress(inputData.type, addressId);
    if (!address) {
      throw new AddressNotFoundException();
    }
  }

  static async deleteAddress(req: Request, res: Response) {
    const inputData = req.body as { type: AddressType };
    const addressId = +req.params.addressId;
    const address   = await addressService.findAddress(inputData.type, addressId);
    if (!address) {
      throw new AddressNotFoundException();
    }
    // await address.destroy;
    await addressService.deleteAddress(address);
    return res.json("Success");
  }

}

