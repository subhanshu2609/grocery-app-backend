import { NextFunction, Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { CouponCreateDto } from "../dtos/coupon/coupon-create.dto";
import { couponService } from "../services/entities/coupon.service";
import { CouponTransformer } from "../transformers/coupon.transformer";
import Ajv from "ajv";
import * as fs from "fs";
import { CouponNotFoundException } from "../exceptions/order/coupon-not-found.exception";
import { cartService } from "../services/entities/cart.service";
import { CannotApplyCouponException } from "../exceptions/order/cannot-apply-coupon.exception";
import { InsufficientCartValueException } from "../exceptions/order/insufficient-cart-value.exception";
import { CartTransformer } from "../transformers/cart.transformer";

export class CouponController {

  static async createCoupon(req: Request, res: Response) {
    req.body.off_amount         = +req.body.off_amount;
    req.body.off_percentage     = +req.body.off_percentage;
    req.body.frequency          = +req.body.frequency;
    req.body.minimum_cart_value = +req.body.minimum_cart_value;

    const inputData = req.body as CouponCreateDto;

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/coupon/coupon-create.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const coupon = await couponService.createCoupon(inputData);

    return res.json({
      data: await new CouponTransformer().transformList(coupon)
    });
  }

  static async listCoupons(req: Request, res: Response) {
    const coupons = await couponService.listCoupons(req.user.id);
    return res.json({
      data: await new CouponTransformer().transformList(coupons)
    });
  }

  static async applyCoupon(req: Request, res: Response) {
    const coupon_id = +req.params.couponId;
    const coupon    = await couponService.showCoupon(coupon_id);
    if (!coupon) {
      throw new CouponNotFoundException();
    }
    const code   = coupon.code;
    const cart   = await cartService.listCart(req.user.id);
    let discount = 0;
    if (coupon.user_id && coupon.user_id !== req.user.id) {
      throw new CannotApplyCouponException();
    }
    if (coupon.minimum_cart_value <= cart.amount) {
      if (coupon.off_amount && coupon.off_percentage) {
        discount = coupon.off_percentage * cart.amount / 100;
        if (coupon.off_amount <= discount) {
          discount = coupon.off_amount;
        }
      } else if (coupon.off_amount) {
        discount = coupon.off_amount;
      } else if (coupon.off_percentage) {
        discount = coupon.off_percentage * cart.amount / 100;
      }
    } else {
      throw new InsufficientCartValueException();
    }
    const newCart = await cart.update({
      discount   : discount,
      coupon_id  : coupon_id,
      coupon_code: code
    });
    return res.json({
      data: await new CartTransformer().transform(newCart)
    });
  }

  static async removeCoupon(req: Request, res: Response) {
    const cart        = await cartService.listCart(+req.user.id);
    const updatedCart = await cartService.resetCart(cart);
    return res.json({
      data: await new CartTransformer().transform(updatedCart)
    });
  }

}
