import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { compareSync } from "bcrypt";
import { UserNotFoundException } from "../exceptions/user/user-not-found.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import Ajv from "ajv";
import * as fs from "fs";
import { employeeService } from "../services/entities/employee.service";
import { EmployeeTransformer } from "../transformers/employee.transformer";
import { EmployeeCreateDto } from "../dtos/employee/employee-create.dto";
import { EmployeeUpdateByAdmin, EmployeeUpdateDto } from "../dtos/employee/employee-update.dto";
import { orderService } from "../services/entities/order.service";
import { OrderTransformer } from "../transformers/order.transformer";
import { OrderNotFoundException } from "../exceptions/order/order-not-found.exception";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { EmployeeNotFoundException } from "../exceptions/user/employee-not-found.exception";
import { Order } from "../models/order.model";
import { OrderStatus } from "../enums/order-status.enum";
import { IncorrectOtpException } from "../exceptions/user/incorrect-otp.exception";
import { historyService } from "../services/entities/history.service";
import { dbService } from "../services/db.service";
import { userService } from "../services/entities/user.service";
import { OrderType } from "../enums/order-type.enum";
import { InsufficientWalletException } from "../exceptions/cart/insufficient-wallet.exception";
import { User } from "../models/user.model";
import { HistoryTransformer } from "../transformers/history.transformer";
import { Employee } from "../models/employee.model";
import { EmployeeCategory } from "../models/employee-category.model";
import { GlobalVariable } from "../models/global-variable.model";
import { PaymentStatus } from "../enums/payment-status.enum";
import { CompactProduct } from "../models/compact-product.model";
import { WalletTransaction } from "../models/wallet-transaction.model";
import { UpdateWalletTypes, WalletTransactionAboutEnum } from "../enums/subscription-types.enum";
import { walletTransactionService } from "../services/entities/wallet-transaction.service";
import { WalletTransactionTransformer } from "../transformers/wallet-transaction.transformer";
import { UserTransformer } from "../transformers/user.transformer";
import { otpService } from "../services/factories/otp.service";
import { PreUser } from "../models/pre-user.model";
import { PaymentModes } from "../enums/payment-modes.enum";

export class EmployeeController {

  static async showEmployeeCategories(req: Request, res: Response) {
    const categories = await EmployeeCategory.findAll();
    return res.json({
      data: categories
    });
  }

  static async showEmployee(req: Request, res: Response) {
    const employeeId = +req.params.employeeId;
    const employee   = await employeeService.show(employeeId);

    if (!employee) {
      throw new UserNotFoundException();
    }

    return res.json({
      data: await new EmployeeTransformer().transform(employee)
    });
  }

  static async showEmployees(req: Request, res: Response) {
    const employees = await Employee.findAll({order: [["name", "asc"]]});
    return res.json({
      data: await new EmployeeTransformer().transformList(employees)
    });
  }

  static async authenticateEmployee(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as { emailOrPhone: string; password: string; };
    // const ajv       = new Ajv();
    // const schema    = JSON.parse(fs.readFileSync("./schema/employee/employee-login.schema.json").toString());
    // try {
    //   const valid = await ajv.validate(schema, inputData);
    // } catch (e) {
    //   throw new UnprocessableEntityException(e.errors);
    // }
    let employee;
    if (inputData.emailOrPhone.indexOf("@") !== -1) {
      employee = await employeeService.showEmployeeByEmail(inputData.emailOrPhone);
    } else {
      employee = await employeeService.showEmployeeByMobile(inputData.emailOrPhone);
    }
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, employee.password);

    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Wrong Password"
      });
    }

    return res.json({
      token: jwt.sign({employee}, "secret"),
      data : await (new EmployeeTransformer()).transform(employee),
    });

  }


  static async createEmployee(req: Request, res: Response, next: NextFunction) {
    req.body.category_id = +req.body.category_id;
    if (req.body.city_id) {
      req.body.city_id = +req.body.city_id;
    }
    const inputData      = req.body as EmployeeCreateDto;
    const ajv            = new Ajv();
    const schema         = JSON.parse(fs.readFileSync("./schema/employee/employee-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (req.employee.category_id != 1) {
      if ((req.employee.category_id === 2 && inputData.category_id != 3) || req.employee.category_id != 2) {
        throw new UnauthorizedException("You are not authorized to perform this action", 301);
      }
    }

    const employee = await employeeService.create(inputData, +req.employee.id);
    return res.json({
      data: await (new EmployeeTransformer()).transform(employee),
    });
  }

  static async myEmployeeProfile(req: Request, res: Response) {
    return res.json({
      employee: await (new EmployeeTransformer()).transform(req.employee),
    });
  }

  static async updateMe(req: Request, res: Response) {
    const employee  = req.employee;
    const inputData = req.body as EmployeeUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/employee/employee-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const updatedEmployee = await employeeService.update(employee, inputData);

    return res.json({
      data: await (new EmployeeTransformer()).transform(updatedEmployee)
    });
  }

  static async updateEmployee(req: Request, res: Response) {
    const employee  = await employeeService.show(+req.params.employeeId);
    const inputData = req.body as EmployeeUpdateByAdmin;
    if (inputData.category_id) {
      inputData.category_id = +inputData.category_id;
    }
    if (inputData.city_id) {
      inputData.city_id = +inputData.city_id;
    }
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/employee/employee-update-by-admin.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const updatedEmployee = await employeeService.update(employee, inputData);

    return res.json({
      data: await (new EmployeeTransformer()).transform(updatedEmployee)
    });
  }

  static async deleteMe(req: Request, res: Response) {
    const employee = req.employee;
    await employeeService.delete(employee);
  }

  static async deleteById(req: Request, res: Response) {
    if (req.employee.category_id != (1 || 2)) {
      throw new UnauthorizedException("You are not Authorized to perform this action", 301);
    }
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    await employeeService.delete(employee);
  }

  // static async setPassword(req: Request, res: Response) {
  //   const mobile   = req.body.mobile_no;
  //   const password = req.body.password;
  //   const employee = await employeeService.showEmployeeByMobile(mobile);
  //   if (!employee || employee.password) {
  //     throw new EmployeeNotFoundException();
  //   }
  //   await employee.update({
  //     password: password
  //   });
  //   return res.json({
  //     data: await new EmployeeTransformer().transform(employee)
  //   });
  // }

  static async deliveryOrders(req: Request, res: Response) {
    if (req.employee.category_id === 1) {
      const unassignedOrders = await Order.findAll({where: {employee_id: null}});
      const assignedOrders   = await Order.findAll({where: {employee_id: {$ne: null}}});
      return res.json({
        data: {
          unassignedOrders: await new OrderTransformer().transformList(unassignedOrders),
          assignedOrders  : await new OrderTransformer().transformList(assignedOrders)
        }
      });
    } else {
      const orders = await orderService.showByEmployee(req.employee.id);
      return res.json({
        data: await new OrderTransformer().transformList(orders),
      });
    }
  }

  static async productCount(req: Request, res: Response) {
    const assignedOrders          = await Order.findAll({where: {employee_id: {$ne: null}}});
    const count: CompactProduct[] = [];
    for (const order of assignedOrders) {
      for (const product of order.products) {
        const previous = count.find((p) => p.product_id === product.product_id);
        if (previous) {
          previous.no_of_units = previous.no_of_units + product.no_of_units;
          const index          = count.indexOf(previous);
          count[index]         = previous;
        } else {
          count.push(product);
        }
      }
    }
    return res.json({
      data: count
    });
  }

  static async assignDeliveryMan(req: Request, res: Response) {
    if (req.employee.category_id > 2) {
      throw new UnauthorizedException("You are not authorized to perform this action", 301);
    }
    const order = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    const employee_id = +req.body.employee_id;
    if (!employee_id) {
      throw new UnauthorizedException("Send Employee ID", 560);
    }
    const employee = await employeeService.show(employee_id);
    console.log(employee.name);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const updatedOrder = await order.update({
      employee_id : employee_id,
      order_status: OrderStatus.ON_THE_WAY
    });
    return res.json({
      data: await new OrderTransformer().transform(updatedOrder)
    });
  }

  static async deliverOrder(req: Request, res: Response) {
    const code = req.body.code;
    if (!code) {
      throw new UnprocessableEntityException("Code Not Found", "Sent code in body", 301);
    }
    const order = await orderService.showOrder(+req.body.order_id);
    if (!order) {
      throw new OrderNotFoundException();
    }
    if (order.delivery_code != code) {
      if (code != "111111") {
        throw new IncorrectOtpException();
      }
    }
    if (order.employee_id != req.employee.id) {
      throw new UnauthorizedException("Not Your Order", 302);
    }
    if (order.order_type == OrderType.SUBSCRIPTION && order.user.wallet < order.amount) {
      throw new InsufficientWalletException();
    }
    let user = order.user;
    let myWallet = user.wallet;
    const transaction = await dbService.getSequelize().transaction();
    try {
      if (order.order_type === OrderType.SUBSCRIPTION) {
        const amount = order.user.secondary_wallet - order.amount;
        user         = await user.update({
          secondary_wallet: amount
        }, {transaction});
      }
      if (!user.earned_referral && user.referred_by) {
        const referred_by_price = await GlobalVariable.findOne({
          where: {
            title: "Referred By"
          }, transaction
        });
        const my_referral_price = await GlobalVariable.findOne({where: {title: "My Referral"}, transaction});
        const referred_by       = await User.findOne({
          where: {
            id: user.referred_by
          },
          transaction
        });
        myWallet += my_referral_price.value;
        await user.update({
          wallet         : myWallet,
          earned_referral: true
        }, {transaction});
        await WalletTransaction.create({
          user_id: user.id,
          type   : UpdateWalletTypes.ADD,
          amount : my_referral_price.value,
          about: WalletTransactionAboutEnum.REFERRAL
        }, {transaction});
        await referred_by.update({wallet: referred_by.wallet + referred_by_price.value}, {transaction});
        await WalletTransaction.create({
          user_id: referred_by.id,
          type   : UpdateWalletTypes.ADD,
          amount : referred_by_price.value,
          about: WalletTransactionAboutEnum.REFERRAL
        }, {transaction});
      }
      const updatedOrder = await orderService.updateOrder(order, {
        delivery_code : code,
        order_status  : OrderStatus.DELIVERED,
        payment_status: PaymentStatus.SUCCESS
      }, transaction);
      const cashback = order.amount * 0.1;
      if (user.card_no) {
        myWallet += cashback;
        await walletTransactionService.createTransaction({
          amount: cashback,
          type: UpdateWalletTypes.ADD,
          about: WalletTransactionAboutEnum.CASHBACK
        }, user.id, transaction);
        await user.update({
          wallet         : myWallet
        }, {transaction});
      }
      const history      = await historyService.createHistory(updatedOrder, updatedOrder.user, transaction);
      await orderService.deleteOrder(updatedOrder, transaction);
      let message;
      if (user.subscription_wallet < order.amount && order.order_type == OrderType.SUBSCRIPTION) {
        message = "Kindly recharge your wallet, else, your order won't be delivered on next date !";
      } else {
        message = "Have a Nice Day !";
      }
      await transaction.commit();
      return res.json({
        data   : await new HistoryTransformer().transform(history),
        message: message
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async delivered(req: Request, res: Response) {
    const filter          = req.query as { query?: string };
    const deliveredOrders = await historyService.delivered(filter);
    return res.json({
      data: await new HistoryTransformer().transformList(deliveredOrders)
    });
  }

  static async cancelled(req: Request, res: Response) {
    const deliveredOrders = await historyService.cancelled();
    return res.json({
      data: await new HistoryTransformer().transformList(deliveredOrders)
    });
  }

  static async rejected(req: Request, res: Response) {
    const deliveredOrders = await historyService.rejected();
    return res.json({
      data: await new HistoryTransformer().transformList(deliveredOrders)
    });
  }

  static async showUserDetails(req: Request, res: Response) {
    const inputData = req.query as { card_no?: string, mobile_no?: string };
    let user;
    if (inputData.card_no) {
      user = await userService.showUserByCardNo(inputData.card_no, false);
    } else if (inputData.mobile_no) {
      user = await userService.showUserByMobile(inputData.mobile_no, false);
    } else {
      throw new UserNotFoundException();
    }
    if (!user) {
      throw new UserNotFoundException();
    }

    if (inputData.mobile_no) {
      const preUser = await userService.preSignup({mobile_no: inputData.mobile_no});
      await otpService.sendOtp(user.mobile_no, preUser.otp);
    }

    return res.json({
      data: await new UserTransformer().transform(user)
    });
  }

  static async payAtRestaurant(req: Request, res: Response) {
    req.body.amount = +req.body.amount;
    const inputData = req.body as { card_no?: string, mobile_no?: string, amount: number, otp: string };
    const ajv            = new Ajv();
    const schema         = JSON.parse(fs.readFileSync("./schema/employee/pay-at-restaurant-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    let user;
    if (inputData.card_no) {
      user = await userService.showUserByCardNo(inputData.card_no, false);
      // throw user;
      if (!user) {
        throw new UserNotFoundException();
      }
      if (user.wallet < +inputData.amount) {
        throw new InsufficientWalletException();
      }
      if (user.card_pin !== inputData.otp) {
        throw new IncorrectOtpException();
      }
    } else if (inputData.mobile_no) {
      user = await userService.showUserByMobile(inputData.mobile_no, false);
      if (!user) {
        throw new UserNotFoundException();
      }
      if (user.wallet < +inputData.amount) {
        throw new InsufficientWalletException();
      }
      const preUser = await PreUser.findOne({
        where: {
          mobile_no: inputData.mobile_no
        }
      });

      if (preUser.otp != inputData.otp) {
        throw new IncorrectOtpException();
      }
    } else {
      throw new UserNotFoundException();
    }

    const transaction = await dbService.getSequelize().transaction();
    try {

      const walletTransaction = await walletTransactionService.createTransaction({
        amount: inputData.amount,
        type: UpdateWalletTypes.SUBTRACT,
        about: WalletTransactionAboutEnum.DINE_IN
      }, user.id, transaction);


      const cashback = inputData.amount * 0.1;
      await user.update({wallet: user.wallet - inputData.amount + cashback}, {transaction});

      await walletTransactionService.createTransaction({
        amount: cashback,
        type: UpdateWalletTypes.ADD,
        about: WalletTransactionAboutEnum.CASHBACK
      }, user.id, transaction);

      await transaction.commit();
      return res.json({
        data: await new WalletTransactionTransformer().transform(walletTransaction)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }
}
