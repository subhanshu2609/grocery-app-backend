import { Request, Response } from "express";
import { OrderCreateDto } from "../dtos/order/order-create.dto";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { orderService } from "../services/entities/order.service";
import { OrderTransformer } from "../transformers/order.transformer";
import { OrderUpdateDto } from "../dtos/order/order-update.dto";
import { OrderNotFoundException } from "../exceptions/order/order-not-found.exception";
import { PaymentStatus } from "../enums/payment-status.enum";
import { historyService } from "../services/entities/history.service";
import { HistoryTransformer } from "../transformers/history.transformer";
import { userService } from "../services/entities/user.service";
import { AddressNotFoundException } from "../exceptions/address/address-not-found.exception";
import { OrderStatus } from "../enums/order-status.enum";
import Ajv from "ajv";
import * as fs from "fs";
import { dbService } from "../services/db.service";
import moment from "moment";
import { CannotCancelOrderException } from "../exceptions/order/cannot-cancel-order.exception";
import { invoiceService } from "../services/factories/invoice.service";
import { Helpers } from "../util/helpers.util";
import { History } from "../models/history.model";
import { User } from "../models/user.model";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { Order } from "../models/order.model";
import { PaymentModes } from "../enums/payment-modes.enum";
import { WalletTransaction } from "../models/wallet-transaction.model";
import { UpdateWalletTypes } from "../enums/subscription-types.enum";
import { otpService } from "../services/factories/otp.service";

export class OrderController {

  static async showOrders(req: Request, res: Response) {
    const orders = await orderService.showIncompleteOrders(req.user.id);
    return res.json({
      data: await new OrderTransformer().transformList(orders)
    });
  }

  static async showHistory(req: Request, res: Response) {
    const orders = await historyService.showHistory(req.user.id);
    return res.json({
      data: await new HistoryTransformer().transformList(orders)
    });
  }

  static async addOrder(req: Request, res: Response) {
    req.body.cart_id = +req.body.cart_id;
    req.body.amount  = +req.body.amount;
    if (req.body.coupon_code) {
      req.body.coupon_code = +req.body.coupon_code;
    }
    const inputData = req.body as OrderCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/order/order-create.schema.json").toString());
    const valid     = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }


    const user = await userService.show(req.user.id);
    if (!user.address || !user.location_id || !user.city_id || !user.pincode) {
      throw new AddressNotFoundException();
    }
    const order = await orderService.addOrder(inputData, req.user, inputData.amount);
    await otpService.sendOtp("9696575771", "Order");
    // await otpService.sendOtp("8178264957", "Order");

    return res.json({
      data: await new OrderTransformer().transform(order)
    });
  }

  static async updateOrder(req: Request, res: Response) {
    if (req.body.employee_id) {
      req.body.employee_id = +req.body.employee_id;
    }
    const inputData = req.body as OrderUpdateDto;
    const orderId   = +req.params.orderId;
    const order     = await orderService.showOrder(orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/order/order-update.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    if (inputData.payment_status === PaymentStatus.FAILURE || inputData.order_status === OrderStatus.CANCELLED) {
      if (inputData.payment_status === PaymentStatus.FAILURE) {
        inputData.order_status = OrderStatus.REJECTED;
      }
      if (inputData.order_status === OrderStatus.CANCELLED) {
        if (moment() >= moment(order.expected_date).subtract(1, "h")) {
          throw new CannotCancelOrderException();
        }
        inputData.payment_status = PaymentStatus.PENDING;
      }
      const transaction = await dbService.getSequelize().transaction();
      try {
        const updatedOrder = await orderService.updateOrder(order, inputData, transaction);
        const history      = await historyService.createHistory(updatedOrder, updatedOrder.user, transaction);
        await orderService.deleteOrder(updatedOrder, transaction);
        if (inputData.order_status === OrderStatus.CANCELLED && updatedOrder.payment_status == PaymentStatus.SUCCESS) {
          const wallet = req.user.wallet + updatedOrder.amount;
          await req.user.update({wallet: wallet}, {transaction});
          await WalletTransaction.create({
            type   : UpdateWalletTypes.ADD,
            amount : updatedOrder.amount,
            user_id: +req.user.id
          }, {transaction});
        }
        await transaction.commit();
        return res.json({
          data: await new HistoryTransformer().transform(history)
        });
      } catch (e) {
        await transaction.rollback();
        throw new UnprocessableEntityException(e);
      }
    }
    const updatedOrder = await orderService.updateOrder(order, inputData);
    return res.json({
      data: await new OrderTransformer().transform(updatedOrder)
    });
  }

  static async itemsOnTheWay(req: Request, res: Response) {
    const orders   = await Order.findAll({
      where: {
        user_id      : req.user.id,
        expected_date: {
          $gte: moment().format("YYYY-MM-DD")
        }
      },
      order: [["expected_date", "asc"]]
    });
    let recentDate = orders[0] ? moment(orders[0].expected_date).format("YYYY-MM-DD") : "";
    let itemsCount = 0;
    let code       = "";
    for (const order of orders) {
      if (moment(order.expected_date).format("YYYY-MM-DD") === recentDate &&
        moment().format("YYYY-MM-DD") <= moment(order.expected_date).format("YYYY-MM-DD")) {
        itemsCount = itemsCount + order.products.length;
        code       = order.delivery_code;
        recentDate = moment(order.expected_date).format("YYYY-MM-DD");
      }
    }
    return res.json({
      count: itemsCount,
      date : recentDate,
      code : code,
    });
  }

  static async rejectOrder(req: Request, res: Response) {
    const order = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    order.order_status = OrderStatus.REJECTED;
    const user         = await userService.show(order.user_id);
    const history      = await historyService.createHistory(order, user);
    await orderService.deleteOrder(order);
    return res.json({
      data: await new HistoryTransformer().transform(history)
    });
  }

  static async downloadInvoiceForUser(req: Request, res: Response) {
    const history = await History.findById(+req.params.historyId);
    const user    = req.user;
    if (user.id != history.user_id) {
      throw new UnauthorizedException("You are not authorized", 301);
    }
    const html = await invoiceService.generatePdf(history, user);
    const pdf  = await Helpers.downloadPDFFromHtml(res, html, "Invoice_" + history.invoice_no + ".pdf");
  }

  static async downloadInvoiceForAdmin(req: Request, res: Response) {
    const history = await History.findById(+req.params.historyId, {include: [User]});
    if (!history) {
      throw new OrderNotFoundException();
    }
    const user    = history.user;
    const html    = await invoiceService.generatePdf(history, user);
    const pdf     = await Helpers.downloadPDFFromHtml(res, html, "Invoice_" + history.invoice_no + ".pdf");
  }

  static async downloadOrderInvoice(req: Request, res: Response) {
    const order = await Order.findById(+req.params.orderId, {include: [User]});
    if (!order) {
      throw new OrderNotFoundException();
    }
    const user  = order.user;
    const html  = await invoiceService.generatePdf(order, user);
    const pdf   = await Helpers.downloadPDFFromHtml(res, html, "Invoice_" + order.order_id + ".pdf");
  }

  static async updateOrderByAdmin(req: Request, res: Response) {
    const order = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    if (req.body.delivery_time) {
      req.body.delivery_time = +req.body.delivery_time;
    }
    const inputData = req.body as OrderUpdateDto;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/order/admin-order-update.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedOrder = await orderService.updateOrder(order, inputData);
    return res.json({
      data: await new OrderTransformer().transform(updatedOrder)
    });
  }

  static async getEnrouteOrderDetails(req: Request, res: Response) {
    const orders = await orderService.showEnrouteOrders(req.user.id);
    return res.json({
      data: orders.map(o => {
        return {
          id: o.id,
          order_id: o.order_id,
          otp: Helpers.replaceUndefinedWithNull(o.delivery_code),
          order_status: o.order_status,
          employee_name: Helpers.replaceUndefinedWithNull(o.employee?.name),
          employee_mobile_no: Helpers.replaceUndefinedWithNull(o.employee?.mobile_no)
        };
      })
    });
  }
}
