import { NextFunction, Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import * as fs from "fs";
import { ProductReviewCreateDto } from "../dtos/product-review/product-review-create.dto";
import Ajv from "ajv";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { productReviewService } from "../services/entities/product-review.service";
import { ProductReviewTransformer } from "../transformers/product-review.transformer";
import { productService } from "../services/entities/product.service";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import { ProductReviewAlreadyExistsException } from "../exceptions/product/product-review-already-exists.exception";
import { CannotDeleteProductReviewException } from "../exceptions/product/cannot-delete-product-review.exception";
import { ProductReviewNotFoundException } from "../exceptions/product/product-review-not-found.exception";

export class ProductReviewController {

  static async viewReviewsByProduct(req: Request, res: Response) {
    const reviews = await productReviewService.listReviewsByProduct(+req.params.productId);
    return res.json({
      data: await new ProductReviewTransformer().transformList(reviews)
    });
  }

  static async createProductReview(req: Request, res: Response) {
    req.body.product_id = +req.body.product_id;
    req.body.rating = +req.body.rating;
    const inputData = req.body as ProductReviewCreateDto;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/product-review/product-review-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const product = await productService.showProductById(inputData.product_id);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const previousReview = await productReviewService.showByProductAndUser(product.id, +req.user.id);
    if (previousReview) {
      throw new ProductReviewAlreadyExistsException();
    }
    const review = await productReviewService.createProductReview(inputData, product, +req.user.id);
    return res.json({
      data: await new ProductReviewTransformer().transform(review)
    });
  }

  static async deleteProductReview(req: Request, res: Response) {
    const review = await productReviewService.showProductReview(+req.params.reviewId);
    if (!review) {
      throw new ProductReviewNotFoundException();
    }
    if (review.user_id != +req.user.id) {
      throw new CannotDeleteProductReviewException();
    }
    await productReviewService.deleteProductReview(review);
    return res.json("success");
  }

}

