import { NextFunction, Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import * as fs from "fs";
import { AttachmentNotFoundException } from "../exceptions/root/attachment-not-found.exception";
import { Attachment } from "../models/attachment.model";
import { ENV_BASE_URL, ENV_S3_BASE_URL } from "../util/secrets.util";
import { Sequelize } from "sequelize";
import { s3Service } from "../services/factories/s3.service";

export class AttachmentController {

  static async listImages(req: Request, res: Response) {
    const images = await Attachment.findAll({
      order: [["priority", "asc"]]
    });
    return res.json({
      data: images
    });
  }

  static async coverImages(req: Request, res: Response) {
    const images = await Attachment.findAll({
      where: {
        priority: {
          $lte: 5,
          $gte: 1
        }
      },
      order: [["priority", "asc"]]
    });
    return res.json({
      data: images
    });
  }

  static async addImage(req: Request, res: Response) {
    req.body.priority = +req.body.priority;
    const inputData   = req.body as { type: string; title: string; priority: number };
    const image       = req.file;
    if (!image) {
      throw new AttachmentNotFoundException();
    }
    let attachment;
    let uploadUrl = "";
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    attachment = await Attachment.create({
      title    : inputData.title,
      priority : inputData.priority,
      image_url: uploadUrl
    });

    return res.json({
      data: attachment
    });
  }

  static async updateImage(req: Request, res: Response) {
    const attachmentId = +req.params.imageId;
    const priority     = +req.body.priority;
    const image        = req.file;
    const attachment   = await Attachment.findById(attachmentId);
    if (!attachment) {
      throw new AttachmentNotFoundException();
    }
    let update;
    let uploadUrl = attachment.image_url;
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, "cover");
      uploadUrl = ENV_S3_BASE_URL + "cover/" + image.originalname;
    }
    update = await attachment.update({
      image_url: uploadUrl,
      priority : priority ? priority : attachment.priority
    });


    return res.json({
      data: update
    });
  }

  static async deleteImage(req: Request, res: Response) {
    const attachmentId = +req.params.imageId;
    const attachment   = await Attachment.findById(attachmentId);
    if (!attachment) {
      throw new AttachmentNotFoundException();
    }
    await attachment.destroy;
    return res.json("Success");
  }

}

