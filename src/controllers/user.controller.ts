import { NextFunction, Request, Response } from "express";
import { userService } from "../services/entities/user.service";
import { UserTransformer } from "../transformers/user.transformer";
import { UserCreateDto } from "../dtos/user/user-create.dto";
import jwt from "jsonwebtoken";
import { UserUpdateDto } from "../dtos/user/user-update.dto";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { UserNotFoundException } from "../exceptions/user/user-not-found.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { PreUser } from "../models/pre-user.model";
import { IncorrectOtpException } from "../exceptions/user/incorrect-otp.exception";
import { AmountNotExistsException } from "../exceptions/user/amount-not-exists.exception";
import { cartService } from "../services/entities/cart.service";
import { dbService } from "../services/db.service";
import Ajv from "ajv";
import * as fs from "fs";
import { User } from "../models/user.model";
import { OtpType } from "../enums/otp-type.enum";
import { otpService } from "../services/factories/otp.service";

export class UserController {

  static async show(req: Request, res: Response) {
    const userId = +req.params.userId;
    const user   = await userService.show(userId);

    if (!user) {
      throw new UserNotFoundException();
    }

    return res.json({
      data: await new UserTransformer().transform(user)
    });
  }

  static async authenticate(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as { mobile_no: string, otp: string };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/user/user-login.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const preUser = await PreUser.findOne({
      where: {
        mobile_no: inputData.mobile_no
      }
    });

    if (!preUser) {
      throw new UnauthorizedException("You are Not Authorized", 401);
    }

    if (preUser.otp != inputData.otp) {
      throw new IncorrectOtpException();
    }
    const user = await userService.showUserByMobile(inputData.mobile_no);
    await preUser.destroy();

    return res.json({
      token: jwt.sign({user}, "secret"),
      user : await (new UserTransformer()).transform(user),
    });

  }


  static async signup(req: Request, res: Response, next: NextFunction) {
    req.body.city_id     = +req.body.city_id;
    req.body.location_id = +req.body.location_id;
    const inputData      = req.body as UserCreateDto;

    const preUser = await PreUser.findOne({
      where: {
        mobile_no: inputData.mobile_no
      }
    });

    if (!preUser) {
      throw new UnauthorizedException("Kindly Request for OTP first", 401);
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/user/user-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (preUser.otp != inputData.otp) {
      throw new IncorrectOtpException();
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const user = await userService.create(inputData, transaction);
      await cartService.createCart(user.id, transaction);
      await preUser.destroy({transaction});
      await transaction.commit();
      return res.json({
        token: jwt.sign({user}, "secret"),
        user : await new UserTransformer().transform(user),
      });
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }

  static async generateOtp(req: Request, res: Response) {
    const inputData = req.body as { mobile_no: string; type: OtpType };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/user/pre-user-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const preUser = await userService.preSignup(inputData);
    // todo: sendOtp

    const user = await userService.showUserByMobile(inputData.mobile_no);
    if (inputData.type === OtpType.LOGIN) {
      if (user) {
        await otpService.sendOtp(inputData.mobile_no, preUser.otp);
        return res.json({data: "OTP Generated, Kindly Login"});
      } else {
        return res.json({data: "OTP Generated, Kindly Register"});
      }
    } else {
      if (user) {
        return res.json({data: "OTP Generated, Kindly Login"});
      } else {
        await otpService.sendOtp(inputData.mobile_no, preUser.otp);
        return res.json({data: "OTP Generated, Kindly Register"});
      }
    }
  }

  static async me(req: Request, res: Response) {
    return res.json({
      user: await (new UserTransformer()).transform(req.user),
    });
  }

  static async updateMe(req: Request, res: Response) {
    if (req.body.city_id) {
      req.body.city_id = +req.body.city_id;
    }
    if (req.body.location_id) {
      req.body.location_id = +req.body.location_id;
    }
    if (req.body.area_id) {
      req.body.area_id = +req.body.area_id;
    }
    const body   = req.body as UserUpdateDto;
    if (body.pincode) {
      body.pincode = body.pincode.trim();
    }
      const user   = req.user;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/user/user-update.schema.json").toString());
    try {
      await ajv.validate(schema, body);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const updatedUser = await userService.update(user, body);

    return res.json({
      token: jwt.sign({user}, "secret"),
      user : await (new UserTransformer()).transform(updatedUser)
    });
  }

  static async updateWallet(req: Request, res: Response) {
    const user = req.user;
    if (req.body.amount) {
      req.body.amount = +req.body.amount;
    }
    const amount = req.body.amount;

    if (!amount) {
      throw new AmountNotExistsException();
    }

    const updatedUser = await userService.updateWallet(user, amount);
    return res.json({
      data: await new UserTransformer().transform(updatedUser)
    });
  }

  static async deleteMe(req: Request, res: Response) {
    const user = req.user;
    await userService.delete(user);
  }


  static async showUsers(req: Request, res: Response) {
    const users = await User.findAll();
    return res.json({
      data: await new UserTransformer().transformList(users)
    });
  }

}
