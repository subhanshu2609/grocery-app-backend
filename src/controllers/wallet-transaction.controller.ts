import { Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { WalletTransaction } from "../models/wallet-transaction.model";
import { WalletTransactionTransformer } from "../transformers/wallet-transaction.transformer";
import { UpdateWalletTypes } from "../enums/subscription-types.enum";
import { dbService } from "../services/db.service";
import { userService } from "../services/entities/user.service";
import { UserNotFoundException } from "../exceptions/user/user-not-found.exception";
import { WalletTransactionCreateDto } from "../dtos/wallet-transaction/wallet-transaction-create.dto";
import { walletTransactionService } from "../services/entities/wallet-transaction.service";

export class WalletTransactionController {

  static async showForAdmin(req: Request, res: Response) {
    const transactions = await WalletTransaction.findAll();
    return res.json({
      data: await new WalletTransactionTransformer().transformList(transactions)
    });
  }

  static async createTransaction(req: Request, res: Response) {
    const inputData   = req.body as WalletTransactionCreateDto;
    const transaction = await dbService.getSequelize().transaction();
    try {
      const user = req.user;
      let change: number;
      if (inputData.type === UpdateWalletTypes.ADD) {
        change = inputData.amount - user.wallet;
      } else {
        change = user.wallet - inputData.amount;
      }
      await user.update({wallet: inputData.amount}, {transaction});

      inputData.amount = change;
      const walletTransaction = await walletTransactionService.createTransaction(inputData, user.id, transaction);
      await transaction.commit();
      return res.json({
        data: await new WalletTransactionTransformer().transform(walletTransaction)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async createTransactionByAdmin(req: Request, res: Response) {
    const inputData   = req.body as WalletTransactionCreateDto;
    const user = await userService.show(+req.params.userId);
    if (!user) {
      throw new UserNotFoundException();
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      let change: number;
      if (inputData.type === UpdateWalletTypes.ADD) {
        change = +inputData.amount + +user.wallet;
      } else {
        change = +user.wallet - +inputData.amount;
      }
      await user.update({wallet: change}, {transaction});

      // Wallet Transaction Create
      inputData.amount = change;
      const walletTransaction = await walletTransactionService.createTransaction(inputData, user.id, transaction);
      await transaction.commit();
      return res.json({
        data: await new WalletTransactionTransformer().transform(walletTransaction)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async showMyTransactions(req: Request, res: Response) {
    const transactions = await walletTransactionService.showByUser(+req.user.id);
    return res.json({
      data: await new WalletTransactionTransformer().transformList(transactions)
    });
  }
}
