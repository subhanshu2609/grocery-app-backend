import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.addColumn("users", "card_no", {
        allowNull : true,
        type      : Sequelize.STRING
      }),
      queryInterface.addColumn("users", "card_pin", {
        allowNull : true,
        type      : Sequelize.STRING
      })
    ]);
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([queryInterface.removeColumn("users", "card_no")]);
  }
};
