import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.addColumn("wallet_transactions", "about", {
        allowNull : true,
        type      : Sequelize.STRING
      })
    ]);
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([queryInterface.removeColumn("wallet_transactions", "about")]);
  }
};
