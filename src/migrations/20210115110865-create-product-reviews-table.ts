import { QueryInterface, SequelizeStatic } from "sequelize";
import { UpdateWalletTypes } from "../enums/subscription-types.enum";
import { Helpers } from "../util/helpers.util";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("product_reviews", {
      id        : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      product_id    : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "products",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      rating: {
        allowNull: false,
        type     : Sequelize.INTEGER
      },
      comment  : {
        allowNull: true,
        type     : Sequelize.STRING
      },
      user_id         : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "users",
          key  : "id"
        },
        onDelete  : "SET NULL"
      },
      createdAt : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("product_reviews"),
    ]);
  }
};
