import { QueryInterface, SequelizeStatic } from "sequelize";
import { UpdateWalletTypes } from "../enums/subscription-types.enum";
import { Helpers } from "../util/helpers.util";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("slots", {
      id        : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      start_time: {
        allowNull: false,
        type     : Sequelize.STRING
      },
      end_time  : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      is_active : {
        allowNull   : false,
        type        : Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("slots"),
    ]);
  }
};
