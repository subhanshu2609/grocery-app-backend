import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.addColumn("orders", "delivery_time", {
        allowNull : true,
        type      : Sequelize.INTEGER
      })
    ]);
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([queryInterface.removeColumn("orders", "delivery_time")]);
  }
};
