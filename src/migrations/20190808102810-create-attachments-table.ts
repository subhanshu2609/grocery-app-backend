import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { PaymentModes } from "../enums/payment-modes.enum";
import { OrderStatus } from "../enums/order-status.enum";
import { PaymentStatus } from "../enums/payment-status.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("attachments", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      title    : {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true
      },
      slug     : {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true
      },
      image_url: {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true
      },
      priority : {
        type        : Sequelize.INTEGER,
        allowNull   : false,
        defaultValue: 0,
        unique      : true
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("faltu"),
    ]);
  }
};
