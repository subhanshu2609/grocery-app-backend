import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { PaymentModes } from "../enums/payment-modes.enum";
import { OrderStatus } from "../enums/order-status.enum";
import { PaymentStatus } from "../enums/payment-status.enum";
import { OrderType } from "../enums/order-type.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("orders", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      order_id        : {
        type     : Sequelize.BIGINT,
        allowNull: false
      },
      user_id         : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "users",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      cart_id         : {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "carts",
          key  : "id"
        }
      },
      delivery_address: {
        allowNull: false,
        type     : Sequelize.STRING
      },
      products        : {
        allowNull: true,
        type     : Sequelize.JSON
      },
      base_amount     : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      amount          : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      delivery_charge : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      discount        : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      employee_id     : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "employees",
          key  : "id"
        },
        onDelete  : "set null"
      },
      coupon_code     : {
        allowNull: true,
        type     : Sequelize.BIGINT
      },
      delivery_code   : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      payment_mode    : {
        allowNull: false,
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(PaymentModes)
      },
      payment_status  : {
        allowNull: false,
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(PaymentStatus)
      },
      order_status    : {
        allowNull: false,
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(OrderStatus)
      },
      order_type      : {
        allowNull: false,
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(OrderType)
      },
      expected_date   : {
        allowNull: true,
        type     : Sequelize.DATEONLY
      },
      transaction_id  : {
        allowNull: true,
        type     : Sequelize.STRING
      },
      transacted_at   : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      slot_time       : {
        allowNull: true,
        type     : Sequelize.STRING
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("orders"),
    ]);
  }
};
