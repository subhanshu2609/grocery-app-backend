import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("coupons", {
      id                : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      user_id           : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "users",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      code              : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      off_amount        : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      off_percentage    : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      minimum_cart_value: {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      frequency         : {
        type     : Sequelize.BIGINT,
        allowNull: true
      },
      created_by        : {
        allowNull : true,
        type      : Sequelize.BIGINT,
        references: {
          model: "employees",
          key  : "id"
        }
      },
      createdAt         : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt         : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt         : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("coupons"),
    ]);
  }
};
