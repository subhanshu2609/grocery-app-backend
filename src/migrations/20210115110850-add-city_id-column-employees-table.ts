import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.addColumn("employees", "city_id", {
        allowNull : false,
        defaultValue: 1,
        type      : Sequelize.BIGINT,
        references: {
          model: "cities",
          key  : "id"
        }
      })
    ]);
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([queryInterface.removeColumn("employees", "city_id")]);
  }
};
