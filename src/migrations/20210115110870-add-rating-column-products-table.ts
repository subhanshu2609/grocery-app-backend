import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.addColumn("products", "rating", {
        allowNull : false,
        type      : Sequelize.FLOAT,
        defaultValue: 0
      })
    ]);
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([queryInterface.removeColumn("products", "rating")]);
  }
};
