import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { SubscriptionTypes } from "../enums/subscription-types.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("subscriptions", {
      id               : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      user_id          : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "users",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      product          : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      start_date       : {
        type     : Sequelize.DATEONLY,
        allowNull: false
      },
      next_delivery    : {
        type     : Sequelize.DATEONLY,
        allowNull: false
      },
      end_date         : {
        type     : Sequelize.DATEONLY,
        allowNull: false
      },
      subscription_type: {
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(SubscriptionTypes),
        allowNull: false
      },
      no_of_days       : {
        type     : Sequelize.INTEGER,
        allowNull: false
      },
      base_amount      : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      amount           : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      delivery_charge  : {
        type        : Sequelize.FLOAT,
        allowNull   : true,
        defaultValue: 0
      },
      delivery_address : {
        type     : Sequelize.STRING,
        allowNull: true,
      },
      discount         : {
        type        : Sequelize.FLOAT,
        allowNull   : true,
        defaultValue: 0
      },
      createdAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("subscriptions"),
    ]);
  }
};
