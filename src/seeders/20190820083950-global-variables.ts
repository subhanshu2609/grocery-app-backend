import { QueryInterface, SequelizeStatic } from "sequelize";
import { dbService } from "../services/db.service";
import { User } from "../models/user.model";
import { Employee } from "../models/employee.model";
import { GlobalVariable } from "../models/global-variable.model";

dbService; // Initialising Sequelize...


const variables: any[] = [
  {
    title: "Referred By",
    value: 29,
  },
  {
    title: "My Referral",
    value: 29,
  },
  {
    title: "Delivery Count",
    value: 0,
  },
  {
    title: "Current Version",
    value: 0
  }
];

export = {
  /**
   * Write code here to seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  up: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return GlobalVariable.bulkCreate(variables);
  },

  /**
   * Write code here for drop seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  down: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return GlobalVariable.truncate();
  }
};
