import { QueryInterface, SequelizeStatic } from "sequelize";
import { dbService } from "../services/db.service";
import { User } from "../models/user.model";
import { Employee } from "../models/employee.model";

dbService; // Initialising Sequelize...


const employees: any[] = [
  {
    name       : "Subhanshu",
    email      : "subhanshu.chaddha2@gmail.com",
    password   : "Subhanshu2@",
    mobile_no  : "9711635385",
    aadhaar_no : "261435385157",
    category_id: 1,
    state      : "New Delhi",
    city       : "Delhi",
    location   : "South Delhi",
    area       : "Kalkaji",
    pincode    : "110019",
    createdBy  : 1
  },
  {
    name       : "Admin",
    email      : "farmetasteagro@gmail.com",
    password   : "Farmtasteagro@2020",
    mobile_no  : "9696575771",
    aadhaar_no : "null",
    category_id: 1,
    state      : "New Delhi",
    city       : "Delhi",
    location   : "South Delhi",
    area       : "Kalkaji",
    pincode    : "201009",
    createdBy  : 1
  }
];

export = {
  /**
   * Write code here to seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  up: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Employee.bulkCreate(employees);
  },

  /**
   * Write code here for drop seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  down: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Employee.truncate();
  }
};
