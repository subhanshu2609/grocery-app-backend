import { QueryInterface, SequelizeStatic } from "sequelize";
import { dbService } from "../services/db.service";
import { User } from "../models/user.model";
import { Employee } from "../models/employee.model";
import { City } from "../models/address/city.model";
import { Location } from "../models/address/location.model";

dbService; // Initialising Sequelize...


const locations: any[] = [
  {
    title    : "Crossing Republic",
    city_id  : 1,
    image_url: " "
  },
  {
    title  : "Gaur City 1",
    city_id: 1
  },
  {
    title  : "Gaur City 2",
    city_id: 1
  },
  {
    title  : "Greater Noida",
    city_id: 1
  },
  {
    title  : "Noida Sector 122",
    city_id: 1
  },
  {
    title  : "Govind Puram",
    city_id: 1
  }
];

export = {
  /**
   * Write code here to seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  up: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Location.bulkCreate(locations);
  },

  /**
   * Write code here for drop seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  down: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Location.truncate();
  }
};
