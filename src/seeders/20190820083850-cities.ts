import { QueryInterface, SequelizeStatic } from "sequelize";
import { dbService } from "../services/db.service";
import { User } from "../models/user.model";
import { Employee } from "../models/employee.model";
import { City } from "../models/address/city.model";

dbService; // Initialising Sequelize...


const cities: any[] = [
  {
    title    : "Delhi/NCR",
    image_url: " "
  }
];

export = {
  /**
   * Write code here to seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  up: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return City.bulkCreate(cities);
  },

  /**
   * Write code here for drop seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  down: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return City.truncate();
  }
};
