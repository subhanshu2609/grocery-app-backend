import { QueryInterface, SequelizeStatic } from "sequelize";
import { dbService } from "../services/db.service";
import { User } from "../models/user.model";
import { Employee } from "../models/employee.model";
import { EmployeeCategory } from "../models/employee-category.model";

dbService; // Initialising Sequelize...


const employeeCategories: any[] = [
  {
    title: "Admin",
  },
  {
    title: "City Admin",
  },
  {
    title: "Delivery Man",
  }
];

export = {
  /**
   * Write code here to seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  up: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return EmployeeCategory.bulkCreate(employeeCategories);
  },

  /**
   * Write code here for drop seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  down: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return EmployeeCategory.truncate();
  }
};
