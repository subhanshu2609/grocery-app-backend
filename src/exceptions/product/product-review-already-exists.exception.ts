import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class ProductReviewAlreadyExistsException extends ModelNotFoundException {

  constructor() {
    super("Product Review Already Exists for the User!", ApiErrorCode.PRODUCT_REVIEW_ALREADY_EXISTS);
  }
}
