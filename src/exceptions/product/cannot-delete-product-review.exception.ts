import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class CannotDeleteProductReviewException extends ModelNotFoundException {

  constructor() {
    super("Cannot Delete Product Review!", ApiErrorCode.CANNOT_DELETE_PRODUCT_REVIEW);
  }
}
