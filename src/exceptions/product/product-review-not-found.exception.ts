import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class ProductReviewNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Product Review Not Found!", ApiErrorCode.PRODUCT_NOT_FOUND);
  }
}
