export enum SubscriptionTypes {
  DAILY       = "daily",
  ALTERNATIVE = "alternative",
  ONCE_A_WEEK = "once a week"
}

export enum UpdateWalletTypes {
  ADD      = "add",
  SUBTRACT = "subtract"
}

export enum WalletTransactionAboutEnum {
  CASHBACK = "cashback",
  ONLINE = "online",
  DINE_IN = "dine in",
  DELIVERY = "delivery",
  REFERRAL = "referral"
}
