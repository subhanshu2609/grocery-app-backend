import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { isUndefined } from "util";
import { Order } from "../models/order.model";
import { User } from "../models/user.model";
import { UserTransformer } from "./user.transformer";
import { Helpers } from "../util/helpers.util";
import { Cart } from "../models/cart.model";
import { CartTransformer } from "./cart.transformer";

export class OrderTransformer extends TransformerAbstract<Order> {

  defaultIncludes = ["user"];

  async includeUser(order: Order): Promise<Dictionary<any>> {
    let user = order.user;
    if (!order.user_id) {
      return null;
    }

    if (isUndefined(user)) {
      user = await order.$get("user") as User;
    }

    return new UserTransformer().transform(user);
  }

  //
  //
  // async includeCart(order: Order): Promise<Dictionary<any>> {
  //   let cart = order.cart;
  //   if (!order.cart_id) {
  //     return null;
  //   }
  //
  //   if (isUndefined(cart)) {
  //     cart = await order.$get("cart") as Cart;
  //   }
  //
  //   return new CartTransformer().transform(cart);
  // }

  protected _map(order: Order): Dictionary<any> {
    return {
      id              : Helpers.replaceUndefinedWithNull(order.id),
      order_id        : Helpers.replaceUndefinedWithNull(order.order_id),
      user_id         : Helpers.replaceUndefinedWithNull(order.user_id),
      delivery_address: Helpers.replaceUndefinedWithNull(order.delivery_address),
      cart_id         : Helpers.replaceUndefinedWithNull(order.cart_id),
      products        : Helpers.replaceUndefinedWithNull(order.products),
      base_amount     : Helpers.replaceUndefinedWithNull(order.base_amount),
      amount          : Helpers.replaceUndefinedWithNull(order.amount),
      delivery_charge : Helpers.replaceUndefinedWithNull(order.delivery_charge),
      discount        : Helpers.replaceUndefinedWithNull(order.discount),
      employee_id     : Helpers.replaceUndefinedWithNull(order.employee_id),
      coupon_code     : Helpers.replaceUndefinedWithNull(order.coupon_code),
      delivery_code   : Helpers.replaceUndefinedWithNull(order.delivery_code),
      payment_mode    : Helpers.replaceUndefinedWithNull(order.payment_mode),
      payment_status  : Helpers.replaceUndefinedWithNull(order.payment_status),
      order_status    : Helpers.replaceUndefinedWithNull(order.order_status),
      order_type      : Helpers.replaceUndefinedWithNull(order.order_type),
      expected_date   : Helpers.replaceUndefinedWithNull(order.expected_date),
      slot_time       : Helpers.replaceUndefinedWithNull(order.slot_time),
      delivery_time   : Helpers.replaceUndefinedWithNull(order.delivery_time),
      transaction_id  : Helpers.replaceUndefinedWithNull(order.transaction_id),
      transacted_at   : Helpers.replaceUndefinedWithNull(order.transacted_at),
      created_at      : Helpers.replaceUndefinedWithNull(order.createdAt),
      updated_at      : Helpers.replaceUndefinedWithNull(order.updatedAt),
      deleted_at      : Helpers.replaceUndefinedWithNull(order.deletedAt)
    };
  }

}
