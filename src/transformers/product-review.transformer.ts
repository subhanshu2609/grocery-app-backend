import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { ProductReview } from "../models/product-review.model";
import { Helpers } from "../util/helpers.util";
import { isUndefined } from "util";
import { User } from "../models/user.model";
import { UserNameOnlyTransformer, UserTransformer } from "./user.transformer";

export class ProductReviewTransformer extends TransformerAbstract<ProductReview> {
  defaultIncludes = ["user"];

  async includeUser(review: ProductReview): Promise<Dictionary<any>> {
    let user = review.user;
    if (!review.user_id) {
      return null;
    }

    if (isUndefined(user)) {
      user = await review.$get("user") as User;
    }

    return new UserNameOnlyTransformer().transform(user);
  }
  protected _map(review: ProductReview): Dictionary<any> {
    return {
      id        : review.id,
      rating    : review.rating,
      comment   : Helpers.replaceUndefinedWithNull(review.comment),
      product_id: review.product_id,
      created_at: review.createdAt,
      updated_at: review.updatedAt,
      deleted_at: review.deletedAt
    };
  }

}
