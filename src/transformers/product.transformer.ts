import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Product } from "../models/product.model";
import { isUndefined } from "util";
import { Helpers } from "../util/helpers.util";

export class ProductCompactTransformer extends TransformerAbstract<Product> {

  protected _map(product: Product): Dictionary<any> {
    let amount;
    if (product.selling_price && product.selling_price > 0) {
      amount = product.selling_price;
    } else {
      amount = product.mrp;
    }
    return {
      __type         : "compact",
      id             : product.id,
      title          : product.title,
      sub_category_id: product.sub_category_id,
      image_url      : product.image_url,
      is_under_gst   : product.is_under_gst,
      base_quantity  : Helpers.replaceUndefinedWithNull(product.base_quantity),
      mrp            : Helpers.replaceUndefinedWithNull(product.mrp),
      selling_price  : Helpers.replaceUndefinedWithNull(product.selling_price),
      city_id        : Helpers.replaceUndefinedWithNull(product.city_id),
      gst_rate       : Helpers.replaceUndefinedWithNull(product.gst_rate),
      // gst_amount     : product.is_under_gst ? Math.ceil(amount * product.gst_rate / 10) / 10 : 0,
      gst_amount     : product.is_under_gst ? product.selling_price + Math.ceil(amount * product.gst_rate / 10) / 10 : product.selling_price,
      is_active      : product.is_active,
      in_stock       : product.in_stock,
      rating : product.rating
    };
  }

}

export class ProductTransformer extends ProductCompactTransformer {
  protected _map(product: Product): Dictionary<any> {

    return {
      ...super._map(product),
      __type    : "full",
      slug      : product.slug,
      created_at: product.createdAt,
      updated_at: product.updatedAt,
      deleted_at: product.deletedAt
    };
  }
}
