import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { isUndefined } from "util";
import { User } from "../models/user.model";
import { UserTransformer } from "./user.transformer";
import { Helpers } from "../util/helpers.util";
import { Cart } from "../models/cart.model";
import { CartTransformer } from "./cart.transformer";
import { History } from "../models/history.model";

export class HistoryTransformer extends TransformerAbstract<History> {

  defaultIncludes = ["user"];

  async includeUser(history: History): Promise<Dictionary<any>> {
    let user = history.user;
    if (!history.user_id) {
      return null;
    }

    if (isUndefined(user)) {
      user = await history.$get("user") as User;
    }

    return new UserTransformer().transform(user);
  }

  protected _map(history: History): Dictionary<any> {
    return {
      id              : Helpers.replaceUndefinedWithNull(history.id),
      order_id        : Helpers.replaceUndefinedWithNull(history.order_id),
      user_id         : Helpers.replaceUndefinedWithNull(history.user_id),
      delivery_address: Helpers.replaceUndefinedWithNull(history.delivery_address),
      products        : Helpers.replaceUndefinedWithNull(history.products),
      base_amount     : Helpers.replaceUndefinedWithNull(history.base_amount),
      amount          : Helpers.replaceUndefinedWithNull(history.amount),
      delivery_charge : Helpers.replaceUndefinedWithNull(history.delivery_charge),
      discount        : Helpers.replaceUndefinedWithNull(history.discount),
      employee_id     : Helpers.replaceUndefinedWithNull(history.employee_id),
      payment_mode    : Helpers.replaceUndefinedWithNull(history.payment_mode),
      payment_status  : Helpers.replaceUndefinedWithNull(history.payment_status),
      delivery_date   : Helpers.replaceUndefinedWithNull(history.delivery_date),
      order_status    : Helpers.replaceUndefinedWithNull(history.order_status),
      order_type      : Helpers.replaceUndefinedWithNull(history.order_type),
      transaction_id  : Helpers.replaceUndefinedWithNull(history.transaction_id),
      transacted_at   : Helpers.replaceUndefinedWithNull(history.transacted_at),
      invoice_no      : Helpers.replaceUndefinedWithNull(history.invoice_no),
      created_at      : Helpers.replaceUndefinedWithNull(history.createdAt),
      updated_at      : Helpers.replaceUndefinedWithNull(history.updatedAt),
      deleted_at      : Helpers.replaceUndefinedWithNull(history.deletedAt)
    };
  }

}
