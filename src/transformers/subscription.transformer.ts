import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Subscription } from "../models/subscription.model";
import { isUndefined } from "util";
import { Helpers } from "../util/helpers.util";
import { User } from "../models/user.model";
import { UserTransformer } from "./user.transformer";

export class SubscriptionTransformer extends TransformerAbstract<Subscription> {

  defaultIncludes = ["user"];

  async includeUser(subscription: Subscription): Promise<Dictionary<any>> {
    let user = subscription.user;
    if (!subscription.user_id) {
      return null;
    }

    if (isUndefined(user)) {
      user = await subscription.$get("user") as User;
    }

    return new UserTransformer().transform(user);
  }

  protected _map(subscription: Subscription): Dictionary<any> {
    return {
      id               : Helpers.replaceUndefinedWithNull(subscription.id),
      user_id          : Helpers.replaceUndefinedWithNull(subscription.user_id),
      product          : Helpers.replaceUndefinedWithNull(subscription.product),
      start_date       : Helpers.replaceUndefinedWithNull(subscription.start_date),
      end_date         : Helpers.replaceUndefinedWithNull(subscription.end_date),
      next_delivery    : Helpers.replaceUndefinedWithNull(subscription.next_delivery),
      subscription_type: Helpers.replaceUndefinedWithNull(subscription.subscription_type),
      no_of_days       : Helpers.replaceUndefinedWithNull(subscription.no_of_days),
      base_amount      : Helpers.replaceUndefinedWithNull(subscription.base_amount),
      amount           : Helpers.replaceUndefinedWithNull(subscription.amount),
      delivery_charge  : Helpers.replaceUndefinedWithNull(subscription.delivery_charge),
      delivery_address : Helpers.replaceUndefinedWithNull(subscription.delivery_address),
      discount         : Helpers.replaceUndefinedWithNull(subscription.discount),
      created_at       : Helpers.replaceUndefinedWithNull(subscription.createdAt),
      updated_at       : Helpers.replaceUndefinedWithNull(subscription.updatedAt),
      deleted_at       : Helpers.replaceUndefinedWithNull(subscription.deletedAt)
    };
  }

}
