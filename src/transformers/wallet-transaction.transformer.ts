import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { isUndefined } from "util";
import { WalletTransaction } from "../models/wallet-transaction.model";
import { User } from "../models/user.model";
import { UserTransformer } from "./user.transformer";
import { Helpers } from "../util/helpers.util";
import { Cart } from "../models/cart.model";
import { CartTransformer } from "./cart.transformer";

export class WalletTransactionTransformer extends TransformerAbstract<WalletTransaction> {

  defaultIncludes = ["user"];

  async includeUser(walletTransaction: WalletTransaction): Promise<Dictionary<any>> {
    let user = walletTransaction.user;
    if (!walletTransaction.user_id) {
      return null;
    }

    if (isUndefined(user)) {
      user = await walletTransaction.$get("user") as User;
    }

    return new UserTransformer().transform(user);
  }

  //
  //
  // async includeCart(walletTransaction: WalletTransaction): Promise<Dictionary<any>> {
  //   let cart = walletTransaction.cart;
  //   if (!walletTransaction.cart_id) {
  //     return null;
  //   }
  //
  //   if (isUndefined(cart)) {
  //     cart = await walletTransaction.$get("cart") as Cart;
  //   }
  //
  //   return new CartTransformer().transform(cart);
  // }

  protected _map(walletTransaction: WalletTransaction): Dictionary<any> {
    return {
      id        : Helpers.replaceUndefinedWithNull(walletTransaction.id),
      user_id   : Helpers.replaceUndefinedWithNull(walletTransaction.user_id),
      amount    : Helpers.replaceUndefinedWithNull(walletTransaction.amount),
      type      : Helpers.replaceUndefinedWithNull(walletTransaction.type),
      about      : Helpers.replaceUndefinedWithNull(walletTransaction.about),
      created_at: Helpers.replaceUndefinedWithNull(walletTransaction.createdAt),
      updated_at: Helpers.replaceUndefinedWithNull(walletTransaction.updatedAt),
      deleted_at: Helpers.replaceUndefinedWithNull(walletTransaction.deletedAt)
    };
  }

}
