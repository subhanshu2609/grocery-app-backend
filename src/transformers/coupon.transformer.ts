import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Helpers } from "../util/helpers.util";
import { Coupon } from "../models/coupon.model";

export class CouponTransformer extends TransformerAbstract<Coupon> {

  protected _map(coupon: Coupon): Dictionary<any> {
    return {
      id                : Helpers.replaceUndefinedWithNull(coupon.id),
      code              : Helpers.replaceUndefinedWithNull(coupon.code),
      user_id           : Helpers.replaceUndefinedWithNull(coupon.user_id),
      off_amount        : Helpers.replaceUndefinedWithNull(coupon.off_amount),
      off_percentage    : Helpers.replaceUndefinedWithNull(coupon.off_percentage),
      minimum_cart_value: Helpers.replaceUndefinedWithNull(coupon.minimum_cart_value),
      frequency         : Helpers.replaceUndefinedWithNull(coupon.frequency),
      created_by        : Helpers.replaceUndefinedWithNull(coupon.created_by),
      created_at        : Helpers.replaceUndefinedWithNull(coupon.createdAt),
      updated_at        : Helpers.replaceUndefinedWithNull(coupon.updatedAt),
      deleted_at        : Helpers.replaceUndefinedWithNull(coupon.deletedAt)
    };
  }

}
