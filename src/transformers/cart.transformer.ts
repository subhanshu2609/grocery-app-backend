import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Cart } from "../models/cart.model";
import { Product } from "../models/product.model";
import { isUndefined } from "util";
import { ProductCompactTransformer, ProductTransformer } from "./product.transformer";
import { Helpers } from "../util/helpers.util";
import { Order } from "../models/order.model";
import { User } from "../models/user.model";
import { UserTransformer } from "./user.transformer";

export class CartTransformer extends TransformerAbstract<Cart> {
  //
  // defaultIncludes = ["product"];
  //
  // async includeProduct(cart: Cart): Promise<Dictionary<any>> {
  //   let product = cart.product;
  //   if (!cart.product_id) {
  //     return null;
  //   }
  //
  //   if (isUndefined(product)) {
  //     product = await cart.$get("product") as Product;
  //   }
  //
  //   return new ProductCompactTransformer().transform(product);
  // }
  // defaultIncludes = ["user"];
  //
  // async includeUser(cart: Cart): Promise<Dictionary<any>> {
  //   let user = cart.user;
  //   if (!cart.user_id) {
  //     return null;
  //   }
  //
  //   if (isUndefined(user)) {
  //     user = await cart.$get("user") as User;
  //   }
  //
  //   return new UserTransformer().transform(user);
  // }

  protected _map(cart: Cart): Dictionary<any> {
    return {
      id             : Helpers.replaceUndefinedWithNull(cart.id),
      products       : Helpers.replaceUndefinedWithNull(cart.products),
      user_id        : Helpers.replaceUndefinedWithNull(cart.user_id),
      amount         : Helpers.replaceUndefinedWithNull(cart.amount),
      base_amount    : Helpers.replaceUndefinedWithNull(cart.base_amount),
      delivery_charge: Helpers.replaceUndefinedWithNull(cart.delivery_charge),
      discount       : Helpers.replaceUndefinedWithNull(cart.discount),
      coupon_code    : Helpers.replaceUndefinedWithNull(cart.coupon_code),
      coupon_id      : Helpers.replaceUndefinedWithNull(cart.coupon_id),
      created_at     : Helpers.replaceUndefinedWithNull(cart.createdAt),
      updated_at     : Helpers.replaceUndefinedWithNull(cart.updatedAt),
      deleted_at     : Helpers.replaceUndefinedWithNull(cart.deletedAt)
    };
  }

}
