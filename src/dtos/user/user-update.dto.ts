
export interface UserUpdateDto {
  name?: string;
  email?: string;
  mobile_no?: string;
  alternate_no?: string;
  state?: string;
  city_id?: number;
  area_id?: number;
  location_id?: number;
  pincode?: string;
  card_no?: string;
  card_pin?: string;
}
