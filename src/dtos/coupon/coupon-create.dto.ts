export interface CouponCreateDto {
  minimum_cart_value: number;
  off_amount: number;
  off_percentage: number;
  code: string;
  user_ids?: number[];
  frequency?: number;
}
