export interface CouponUpdateDto {
  minimum_cart_value?: number;
  off_amount?: number;
  off_percentage?: number;
  code?: string;
  user_ids?: number[];
}
