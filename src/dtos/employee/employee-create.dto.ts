export interface EmployeeCreateDto {
  name: string;
  email: string;
  password: string;
  aadhaar_no: string;
  driver_license: string;
  category_id: number;
  state: string;
  city: string;
  location: string;
  area: string;
  address: string;
  mobile_no: string;
  pincode: string;
  city_id: number;
}
