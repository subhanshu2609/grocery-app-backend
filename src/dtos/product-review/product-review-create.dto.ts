export interface ProductReviewCreateDto {
  comment?: string;
  product_id: number;
  rating: number;
}