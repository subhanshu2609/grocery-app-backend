import { SubscriptionTypes } from "../../enums/subscription-types.enum";

export interface SubscriptionCreateDto {
  product_id: number;
  no_of_units: number;
  start_date: Date;
  end_date: Date;
  subscription_type: SubscriptionTypes;
}
