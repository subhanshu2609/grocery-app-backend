import { ImageUploadType } from "../../enums/image-upload-type.enum";

export interface ProductUpdateDto {
  title?: string;
  is_under_gst?: boolean;
  base_quantity?: string;
  gst_rate?: number;
  mrp?: number;
  selling_price?: number;
  city_id?: number;
  type?: ImageUploadType;
  is_active?: boolean;
  in_stock?: boolean;
  rating?: number;
}
