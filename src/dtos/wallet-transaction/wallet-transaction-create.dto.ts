import { UpdateWalletTypes } from "../../enums/subscription-types.enum";

export interface WalletTransactionCreateDto {
  amount: number;
  type: UpdateWalletTypes;
  about?: string;
}
