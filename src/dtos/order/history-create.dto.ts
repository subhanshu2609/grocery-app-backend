import { PaymentModes } from "../../enums/payment-modes.enum";
import { PaymentStatus } from "../../enums/payment-status.enum";
import { OrderStatus } from "../../enums/order-status.enum";
import { CompactProduct } from "../../models/compact-product.model";
import { OrderType } from "../../enums/order-type.enum";

export interface HistoryCreateDto {
  order_id: number;
  user_id: number;
  products: CompactProduct[];
  amount: number;
  base_amount: number;
  delivery_charge: number;
  discount: number;
  delivery_address: string;
  employee_id?: number;
  payment_mode: PaymentModes;
  payment_status: PaymentStatus;
  order_status: OrderStatus;
  order_type: OrderType;
  delivery_date?: Date;
  transaction_id?: string;
  transacted_at?: Date;
  invoice_no?: number;
}
