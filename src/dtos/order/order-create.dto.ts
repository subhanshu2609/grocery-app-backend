import { PaymentModes } from "../../enums/payment-modes.enum";

export interface OrderCreateDto {
  cart_id: number;
  payment_mode: PaymentModes;
  amount: number;
  slot_time: string;
}
