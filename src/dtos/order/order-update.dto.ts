import { PaymentStatus } from "../../enums/payment-status.enum";
import { OrderStatus } from "../../enums/order-status.enum";

export interface OrderUpdateDto {
  employee_id?: number;
  payment_status?: PaymentStatus;
  order_status?: OrderStatus;
  transaction_id?: string;
  transacted_at?: Date;
  delivery_code?: string;
  delivery_time?: number;
}
