import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import * as _ from "lodash";
import { dbService } from "./services/db.service";
import { ENV_APP_PORT_REST } from "./util/secrets.util";
import { userService } from "./services/entities/user.service";
import { mattermostService } from "./services/mattermost.service";
import { UserController } from "./controllers/user.controller";
import { userMiddleware } from "./middlewares/user.middleware";
import { errorHandler } from "./handlers/error-handler";
import { upload } from "./services/factories/multer.service";
import { AddressController } from "./controllers/address.controller";
import { ProductController } from "./controllers/product.controller";
import { CartController } from "./controllers/cart.controller";
import { OrderController } from "./controllers/order.controller";
import { CouponController } from "./controllers/coupon.controller";
import { SubscriptionController } from "./controllers/subscription.controller";
import { AttachmentController } from "./controllers/attachment.controller";
import { employeeMiddleware } from "./middlewares/employee.middleware";
import { EmployeeController } from "./controllers/employee.controller";
import { adminMiddleware } from "./middlewares/admin.middleware";
import { cronService } from "./services/factories/cron.service";
import { GlobalVariableController } from "./controllers/global-variable.controller";
import { WalletTransactionController } from "./controllers/wallet-transaction.controller";
import { SlotController } from "./controllers/slot.controller";
import { adminManagerMiddleware } from "./middlewares/admin-manager.middleware";
import { ProductReviewController } from "./controllers/product-review.controller";


// Create Express server
const app = express();

// Entities
userService;

// Factories
// cryptService;
// jwtService;
// s3Service;
// validatorService;
// snsService;

// Others
dbService;
mattermostService;
cronService;

// Express configuration
app.set("port", process.env.PORT || ENV_APP_PORT_REST);
// app.use(snsHeaderMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// CORS Setup
const allowedOrigins = ["*",
  "http://localhost:*",
  "http://localhost:3000",
  "http://localhost:4200",
  "http://localhost:5500",
  "http://localhost:4200/admin-home",
  "http://ec2-3-7-184-113.ap-south-1.compute.amazonaws.com:3000",
  "http://ec2-3-7-184-113.ap-south-1.compute.amazonaws.com:3000/",
  "http://ec2-3-7-184-113.ap-south-1.compute.amazonaws.com",
  "http://ec2-3-7-184-113.ap-south-1.compute.amazonaws.com/",
  "http://farmtaste.s3.ap-south-1.amazonaws.com/",
  "http://farmtaste.s3.ap-south-1.amazonaws.com",
  "http://farmtaste.s3.ap-south-1.amazonaws.com/index.html",
  "http://farmtaste.s3-website.ap-south-1.amazonaws.com",
  "http://pawsitivity.sidcredible.com/",
  "http://pawsitivity.sidcredible.com",
  "http://expecto.s3.amazonaws.com/index.html",
  "http://expecto.s3.amazonaws.com/",
  "http://expecto.s3.amazonaws.com",
  "http://old-farmtaste.s3.us-east-1.amazonaws.com/index.html",
  "http://old-farmtaste.s3.us-east-1.amazonaws.com/",
  "http://old-farmtaste.s3.us-east-1.amazonaws.com"
];

app.use(cors({
  origin : (origin, callback) => {
    if (!origin || _.includes(allowedOrigins, origin)) {
      callback(undefined, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  methods: [
    "GET",
    "HEAD",
    "PUT",
    "PATCH",
    "POST",
    "DELETE"
  ]
}));
app.options("*");

// Static Public Content
app.use("/public", express.static("./public", {maxAge: 31557600000}));

// Global Middleware(s)

/**
 * Primary app routes.
 */


// AUTH
app.post("/generate-otp", errorHandler(UserController.generateOtp));
app.post("/signup", errorHandler(UserController.signup));
app.post("/login", errorHandler(UserController.authenticate));
app.post("/login-employee", errorHandler(EmployeeController.authenticateEmployee));


// USER
app.get("/me", [userMiddleware], errorHandler(UserController.me));
app.put("/me", [userMiddleware], errorHandler(UserController.updateMe));
app.delete("/me", [userMiddleware], errorHandler(UserController.deleteMe));
app.put("/update-wallet", [userMiddleware], errorHandler(UserController.updateWallet));


// ADDRESSES
app.post("/address", [employeeMiddleware, upload.single("image")], errorHandler(AddressController.addAddresses));
app.delete("/address/:addressId([0-9]+)", [employeeMiddleware], errorHandler(AddressController.deleteAddress));
app.get("/cities", errorHandler(AddressController.listCities));
app.get("/locations/:cityId([0-9]+)", errorHandler(AddressController.listLocations));
app.get("/areas/:locationId([0-9]+)", errorHandler(AddressController.listAreas));


// CATEGORY
app.get("/productCategories", errorHandler(ProductController.listProductCategories));
app.post("/productCategories", [employeeMiddleware], errorHandler(ProductController.createProductCategory));
app.put("/productCategory/:categoryId([0-9]+)", [employeeMiddleware], errorHandler(ProductController.updateProductCategory));
app.delete("/productCategory/:categoryId([0-9]+)", [employeeMiddleware], errorHandler(ProductController.deleteProductCategory));


// SUB CATEGORY
app.get("/productSubCategories/:categoryId([0-9]+)", errorHandler(ProductController.listProductSubCategories));
app.post("/productSubCategories", [employeeMiddleware, upload.single("image")], errorHandler(ProductController.createProductSubCategory));
app.put("/productSubCategory/:subCategoryId([0-9]+)", [employeeMiddleware, upload.single("image")], errorHandler(ProductController.updateProductSubCategory));
app.delete("/productSubCategory/:subCategoryId([0-9]+)", [employeeMiddleware], errorHandler(ProductController.deleteProductSubCategory));


// PRODUCT
app.get("/products/:subCategoryId([0-9]+)", [userMiddleware], errorHandler(ProductController.listProducts));
app.get("/products", [userMiddleware], errorHandler(ProductController.allProducts));
app.get("/products-admin", [employeeMiddleware], errorHandler(ProductController.productForAdmin));
app.get("/products-sc-admin/:subCategoryId([0-9]+)", [employeeMiddleware], errorHandler(ProductController.productBySCForAdmin));
app.post("/products", [employeeMiddleware, upload.single("image")], errorHandler(ProductController.createProduct));
app.put("/product/:productId([0-9]+)", [employeeMiddleware, upload.single("image")], errorHandler(ProductController.updateProduct));
app.delete("/product/:productId([0-9]+)", [employeeMiddleware], errorHandler(ProductController.deleteProduct));


// CART
app.get("/my-cart", [userMiddleware], errorHandler(CartController.listCart));
app.put("/add-to-cart", [userMiddleware], errorHandler(CartController.addToCart));
app.delete("/empty-cart", [userMiddleware], errorHandler(CartController.emptyCart));
app.get("/product-counter/:productId([0-9]+)", [userMiddleware], errorHandler(CartController.countProductOfCart));


// SUBSCRIPTION
app.get("/my-subscriptions", [userMiddleware], errorHandler(SubscriptionController.viewMySubscription));
app.post("/add-to-subscription", [userMiddleware], errorHandler(SubscriptionController.addToSubscription));
app.post("/subscription-to-orders", errorHandler(SubscriptionController.addToOrders));
app.delete("/subscription/:subscriptionId([0-9]+)", [userMiddleware], errorHandler(SubscriptionController.cancelSubscription));


// COUPON
app.get("/coupons", [userMiddleware], errorHandler(CouponController.listCoupons));
app.post("/coupons", [userMiddleware], errorHandler(CouponController.createCoupon));
app.put("/apply-coupon/:couponId([0-9]+)", [userMiddleware], errorHandler(CouponController.applyCoupon));
app.put("/remove-coupon", [userMiddleware], errorHandler(CouponController.removeCoupon));


// ORDER
app.get("/orders", [userMiddleware], errorHandler(OrderController.showOrders));
app.post("/orders", [userMiddleware], errorHandler(OrderController.addOrder));
app.put("/orders/:orderId([0-9]+)", [userMiddleware], errorHandler(OrderController.updateOrder));
app.get("/history", [userMiddleware], errorHandler(OrderController.showHistory));
app.get("/items-on-the-way", [userMiddleware], errorHandler(OrderController.itemsOnTheWay));
app.get("/processing-orders", [userMiddleware], errorHandler(OrderController.getEnrouteOrderDetails));


// IMAGES
app.get("/images", [employeeMiddleware], errorHandler(AttachmentController.listImages));
app.get("/cover-images", [userMiddleware], errorHandler(AttachmentController.coverImages));
app.post("/images", [employeeMiddleware, upload.single("image")], errorHandler(AttachmentController.addImage));
app.put("/image/:imageId([0-9]+)", [employeeMiddleware, upload.single("image")], errorHandler(AttachmentController.updateImage));
app.delete("/image/:imageId([0-9]+)", [employeeMiddleware], errorHandler(AttachmentController.deleteImage));


// EMPLOYEES
app.get("/employee-categories", [employeeMiddleware], errorHandler(EmployeeController.showEmployeeCategories));
app.get("/my-employee-profile", [employeeMiddleware], errorHandler(EmployeeController.myEmployeeProfile));
app.post("/employees", [employeeMiddleware], errorHandler(EmployeeController.createEmployee));
app.put("/employees", [employeeMiddleware], errorHandler(EmployeeController.updateMe));
app.put("/update-employee/employeeId", [adminMiddleware], errorHandler(EmployeeController.updateMe));
app.put("/deliver-order", [employeeMiddleware], errorHandler(EmployeeController.deliverOrder));
app.delete("/employee/:employeeId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.deleteById));
app.get("/delivery-order", [employeeMiddleware], errorHandler(EmployeeController.deliveryOrders));


// ADMIN
app.put("/assign-delivery-man/:orderId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.assignDeliveryMan));
app.put("/admin/order/:orderId([0-9]+)", [adminMiddleware], errorHandler(OrderController.updateOrderByAdmin));
app.get("/users", [adminMiddleware], errorHandler(UserController.showUsers));
app.get("/employees", [adminMiddleware], errorHandler(EmployeeController.showEmployees));
app.put("/employees/:employeeId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.updateEmployee));
app.get("/delivered", [adminMiddleware], errorHandler(EmployeeController.delivered));
app.get("/cancelled", [adminMiddleware], errorHandler(EmployeeController.cancelled));
app.get("/rejected", [adminMiddleware], errorHandler(EmployeeController.rejected));
app.put("/reject-order/:orderId([0-9]+)", [adminMiddleware], errorHandler(OrderController.rejectOrder));
app.get("/current-subscriptions", [adminMiddleware], errorHandler(SubscriptionController.viewCurrentSubscriptions));
app.get("/all-subscriptions", [adminMiddleware], errorHandler(SubscriptionController.viewAllSubscriptions));
app.get("/all-transactions", [adminMiddleware], errorHandler(WalletTransactionController.showMyTransactions));
app.get("/product-count", [adminMiddleware], errorHandler(EmployeeController.productCount));


// GLOBAL VARIABLES
app.get("/current-version", errorHandler(GlobalVariableController.showVersion));
app.get("/variables", errorHandler(GlobalVariableController.show));
app.post("/variables", [adminMiddleware], errorHandler(GlobalVariableController.createVariable));
app.put("/variable/:variableId([0-9]+)", [adminMiddleware], errorHandler(GlobalVariableController.updateVariables));
app.delete("/variable/:variable([0-9]+)", [adminMiddleware], errorHandler(GlobalVariableController.deleteVariable));


// WALLET TRANSACTION
app.get("/my-transactions", [userMiddleware], errorHandler(WalletTransactionController.showMyTransactions));
app.post("/transactions", [userMiddleware], errorHandler(WalletTransactionController.createTransaction));
app.post("/wallet/:userId([0-9]+)", [adminMiddleware], errorHandler(WalletTransactionController.createTransactionByAdmin));


// INVOICE
app.get("/invoice/:historyId([0-9]+)", [userMiddleware], errorHandler(OrderController.downloadInvoiceForUser));
app.post("/invoice-for-admin/:historyId([0-9]+)", [adminMiddleware], errorHandler(OrderController.downloadInvoiceForAdmin));
app.post("/order-invoice/:orderId([0-9]+)", [adminMiddleware], errorHandler(OrderController.downloadOrderInvoice));

// SLOT
app.get("/slots", [userMiddleware], errorHandler(SlotController.viewSlots));
app.get("/all-slots", [adminMiddleware], errorHandler(SlotController.viewAllSlots));
app.post("/slots", [adminMiddleware], errorHandler(SlotController.createSlot));
app.delete("/slot/:slotId([0-9]+)", [adminMiddleware], errorHandler(SlotController.deleteSlot));

// RESTAURANT APP
app.get("/user", [adminManagerMiddleware], errorHandler(EmployeeController.showUserDetails));
app.post("/pay/restaurant", [adminManagerMiddleware], errorHandler(EmployeeController.payAtRestaurant));

// PRODUCT REVIEWS
app.get("/product/:productId([0-9]+)/reviews", [userMiddleware], errorHandler(ProductReviewController.viewReviewsByProduct));
app.post("/product/review", [userMiddleware], errorHandler(ProductReviewController.createProductReview));

app.get("*", (req, res) => {
  res.send({data: "Works"});
});


export default app;
