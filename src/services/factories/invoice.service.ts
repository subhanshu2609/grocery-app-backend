import logger from "../../util/logger.util";
import * as fs from "fs";
import * as path from "path";
import appRoot from "app-root-path";
import * as handlebars from "handlebars";
import { Order } from "../../models/order.model";
import { User } from "../../models/user.model";
import moment from "moment";
import { OrderStatus } from "../../enums/order-status.enum";

class InvoiceService {
  constructor() {
    logger.silly("[N-FT] InvoiceService");
  }


  static getInstance(): InvoiceService {
    return new InvoiceService();
  }

  async generatePdf(order: any, user: User) {
    const company = {
      name   : "Farm Taste",
      address: "28, Farm Taste Agro Farming LLP, Ghazipur, 233001",
      email  : "support@farmtaste.co.in",
      phone  : "+91-8178264957, +91-8750185501"
    };
    let gst       = 0;
    for (const product of order.products) {
      gst = gst + (product.gst_amount * product.no_of_units);
    }
    const invoice_order = {
      order_id       : order.order_id,
      status         : order.order_status,
      payment_mode   : order.payment_mode,
      invoice_no     : order.invoice_no ? "UPFT" + order.invoice_no : "",
      products       : order.products,
      total          : Math.round(order.amount * 10) / 10,
      total_gst      : gst,
      delivery_charge: order.delivery_charge,
      discount: order.discount,
      gst_id         : "09AAHFF1066C1ZQ",
      date           : order.order_status === OrderStatus.DELIVERED ? order.delivery_date : moment().format("DD-MM-YYYY")
    };
    const buyer         = {
      name   : user.name,
      address: order.delivery_address,
      email  : user.email,
      phone  : user.mobile_no
    };
    handlebars.registerHelper("inc", function (value, options) {
      return parseInt(value) + 1;
    });
    handlebars.registerHelper("divBy2", function (value, options) {
      return (parseFloat(value) * 100 / 2) / 100;
    });
    handlebars.registerHelper("dec", function (value1, value2) {
      return parseFloat(value1) - parseFloat(value2);
    });
    handlebars.registerHelper("add", function (value1, value2) {
      return parseFloat(value1) + parseFloat(value2);
    });
    handlebars.registerHelper("mul", function (value1, value2) {
      return parseFloat(value1) * parseFloat(value2);
    });
    handlebars.registerHelper("round", function (value, options) {
      return Math.round(value * 10) / 10;
    });
    handlebars.registerHelper("round2digits", function (value, options) {
      return (value * 100) / 100;
    });
    const templateHtml = fs.readFileSync(path.join(appRoot.path, "public/views/new-invoice-template.hbs"), "utf8");
    const template     = handlebars.compile(templateHtml, {
      explicitPartialContext: true,
      strict                : false
    });

    const html = template({
      order  : invoice_order,
      company: company,
      buyer  : buyer
    }, {
      allowProtoMethodsByDefault   : true,
      allowProtoPropertiesByDefault: true
    });
    // const pdf      = await Helpers.createPdf(html);

    return html;
  }

}

export const invoiceService = InvoiceService.getInstance();



