import axios from "axios";
import logger from "../../util/logger.util";

class OtpService {
  constructor() {
    logger.silly("[N-FT] OtpService");
  }


  static getInstance(): OtpService {
    return new OtpService();
  }

  async sendOtp(mobile_no: string, otp: string) {
    axios({
      method             : "POST",
      url                : "https://www.fast2sms.com/dev/bulk",
      headers            : {
        authorization: "YVkdzGMSXLQTv9hewco4mK65EBWuIPfOR8sl201rpUHJix3ZygJpb4oCQ3nGOStY0wgvr7uxTNKsB2qF"
      },
      data               : {
        sender_id       : "FSTSMS",
        language        : "english",
        route           : "qt",
        message         : 28934,
        numbers         : mobile_no,
        variables       : "{#AA#}",
        variables_values: otp
      },
      timeoutErrorMessage: "Error"
    });
  }

}

export const otpService = OtpService.getInstance();



