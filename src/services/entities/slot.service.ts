import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { Slot } from "../../models/slot.model";
import { SlotCreateDto } from "../../dtos/slot/slot-create.dto";

class SlotService {
  constructor() {
    logger.silly("[N-FT] SlotService");
  }

  static getInstance(): SlotService {
    return new SlotService();
  }

  async showSlot(slotId: number): Promise<Slot> {
    return Slot.findById(slotId);
  }

  async listSlots(): Promise<Slot[]> {
    return Slot.findAll({
      where: {
        is_active: true
      }
    });
  }

  async listAllSlots(): Promise<Slot[]> {
    return Slot.findAll();
  }

  async createSlot(data: SlotCreateDto): Promise<Slot> {
    return Slot.create(data);
  }

  async deleteSlot(slot: Slot): Promise<any> {
    return slot.destroy();
  }
}

export const slotService = SlotService.getInstance();
