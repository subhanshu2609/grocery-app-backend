import logger from "../../util/logger.util";
import { UserCreateDto } from "../../dtos/user/user-create.dto";
import { isUndefined } from "util";
import { UserUpdateDto } from "../../dtos/user/user-update.dto";
import { Helpers } from "../../util/helpers.util";
import { User } from "../../models/user.model";
import { PreUser } from "../../models/pre-user.model";
import { Transaction } from "sequelize";

class UserService {
  private readonly LIMIT = 20;

  private constructor() {
    logger.silly("[N-FT] UserService");
  }

  static getInstance(): UserService {
    return new UserService();
  }

  async preSignup(data: { mobile_no: string }): Promise<PreUser> {
    const token   = Helpers.generateRandomString(5, {
      includeLowerCase: false,
      includeNumbers  : true,
      includeUpperCase: false
    });
    const preUser = await PreUser.findOne({
      where: {
        mobile_no: data.mobile_no
      }
    });
    if (preUser) {
      return preUser.update({otp: token});
    }
    return PreUser.create({
      mobile_no: data.mobile_no,
      otp      : token
    });
  }

  async create(data: UserCreateDto, transaction?: Transaction): Promise<User> {
    let referrer: User;
    if (data.referral_code) {
      referrer = await User.findOne({
        where: {
          referral_code: data.referral_code
        }
      });
      return User.create({
        ...data,
        referred_by  : referrer ? referrer.id : null,
        referral_code: await Helpers.generateRandomString(6, {
          includeNumbers  : true,
          includeUpperCase: true
        })
      }, {transaction});
    }
    return User.create({
      ...data,
      referral_code: await Helpers.generateRandomString(6, {
        includeNumbers  : true,
        includeUpperCase: true
      })
    }, {transaction});
  }

  async show(userId: number, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        id: userId
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async showUserByEmail(email: string, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        email: email
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async showUserByMobile(mobile: string, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        mobile_no: mobile
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async showUserByCardNo(cardNo: string, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        card_no: cardNo
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async update(user: User, data: UserUpdateDto): Promise<User> {
    if (data.card_no) {
      data.card_pin = Helpers.generateRandomString(6, {
        includeNumbers  : true,
        includeUpperCase: false
      });
    }
    return user.update(data);
  }

  async updateWallet(user: User, amount: number, transaction?: Transaction): Promise<User> {
    return user.update({
      wallet: amount
    }, {transaction});
  }

  async delete(user: User): Promise<any> {
    await user.destroy();
  }
}

export const userService = UserService.getInstance();
