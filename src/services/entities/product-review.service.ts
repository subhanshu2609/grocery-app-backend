import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { ProductReview } from "../../models/product-review.model";
import { ProductReviewCreateDto } from "../../dtos/product-review/product-review-create.dto";
import _, { round } from "lodash";
import { productService } from "./product.service";
import { Product } from "../../models/product.model";

class ProductReviewService {
  constructor() {
    logger.silly("[N-FT] ProductReviewService");
  }

  static getInstance(): ProductReviewService {
    return new ProductReviewService();
  }

  async showProductReview(reviewId: number): Promise<ProductReview> {
    return ProductReview.findById(reviewId);
  }

  async showByProductAndUser(productId: number, userId: number): Promise<ProductReview> {
    return ProductReview.findOne({
      where: {
        product_id: productId,
        user_id: userId
      }
    });
  }

  async listReviewsByProduct(productId: number): Promise<ProductReview[]> {
    return ProductReview.findAll({
      where: {
        product_id: productId
      }
    });
  }

  async createProductReview(data: ProductReviewCreateDto, product: Product, userId: number): Promise<ProductReview> {
    const review = await ProductReview.create({
      ...data,
      user_id: userId
    });
    const previousReviews = await this.listReviewsByProduct(data.product_id);
    let sum = 0;
    previousReviews.forEach(p => {
      sum += p.rating;
    });
    await productService.updateProduct(product, {
      rating: round(sum * 10 / previousReviews.length) / 10
    });
    return review;
  }

  async deleteProductReview(review: ProductReview): Promise<any> {
    return review.destroy();
  }
}

export const productReviewService = ProductReviewService.getInstance();
