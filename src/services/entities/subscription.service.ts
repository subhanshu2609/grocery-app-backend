import logger from "../../util/logger.util";
import { Product } from "../../models/product.model";
import { User } from "../../models/user.model";
import { CompactProduct } from "../../models/compact-product.model";
import { Subscription } from "../../models/subscription.model";
import { SubscriptionCreateDto } from "../../dtos/subscription/subscription-create.dto";
import moment from "moment";
import { StartDateNotValidException } from "../../exceptions/cart/start-date-not-valid.exception";
import { InsufficientDaysOfSubscriptionException } from "../../exceptions/cart/insufficient-days-of-subscription.exception";
import { SubscriptionTypes, UpdateWalletTypes } from "../../enums/subscription-types.enum";
import { InsufficientWalletException } from "../../exceptions/cart/insufficient-wallet.exception";
import { Order } from "../../models/order.model";
import { addressService } from "./address.service";
import { Helpers } from "../../util/helpers.util";
import { PaymentStatus } from "../../enums/payment-status.enum";
import { OrderStatus } from "../../enums/order-status.enum";
import { PaymentModes } from "../../enums/payment-modes.enum";
import { Cart } from "../../models/cart.model";
import { dbService } from "../db.service";
import { UnprocessableEntityException } from "../../exceptions/root/unprocessable-entity.exception";
import { OrderType } from "../../enums/order-type.enum";
import { preprocessDirectives } from "tslint/lib/verify/parse";
import { WalletTransaction } from "../../models/wallet-transaction.model";

class SubscriptionService {
  constructor() {
    logger.silly("[N-FT] SubscriptionService");
  }

  static getInstance(): SubscriptionService {
    return new SubscriptionService();
  }

  async show(subscriptionId: number): Promise<Subscription> {
    return Subscription.findById(subscriptionId, {include: [User]});
  }

  async showByUser(userId: number): Promise<Subscription[]> {
    return Subscription.findAll({
      where: {
        user_id: userId
      }
    });
  }

  async addToSubscription(data: SubscriptionCreateDto, product: Product, user: User): Promise<Subscription> {
    if (moment(data.start_date) < moment()) {
      throw new StartDateNotValidException();
    }
    let days = moment(data.end_date).diff(moment(data.start_date), "days");
    if (data.subscription_type === SubscriptionTypes.ALTERNATIVE) {
      days = days / 2;
    } else if (data.subscription_type === SubscriptionTypes.ONCE_A_WEEK) {
      days = Math.floor(days / 7);
    }
    if (days < 5) {
      throw new InsufficientDaysOfSubscriptionException();
    }

    let amount;
    if (product.selling_price && product.selling_price > 0) {
      amount = product.selling_price;
    } else {
      amount = product.mrp;
    }
    const compactProduct: CompactProduct = {
      product_id   : data.product_id,
      title        : product.title,
      base_rate    : product.mrp,
      rate         : product.selling_price,
      no_of_units  : data.no_of_units,
      image_url    : product.image_url,
      gst_amount   : product.is_under_gst ? product.gst_rate * amount : 0,
      base_quantity: product.base_quantity
    };

    const rate     = product.is_under_gst ? amount * (100 + product.gst_rate) / 100 : amount;
    const total    = rate * compactProduct.no_of_units * days;
    const discount = 0;
    // const discount = 5 * total / 100;
    if (total > user.wallet) {
      throw new InsufficientWalletException();
    }
    await user.update({
      wallet             : user.wallet - total,
      subscription_wallet: user.subscription_wallet + total
    });
    await WalletTransaction.create({
      user_id: user.id,
      type   : UpdateWalletTypes.SUBTRACT,
      amount : total,
    });
    const city     = await addressService.findCity(user.city_id);
    const location = await addressService.findLocation(user.location_id);

    return Subscription.create({
      ...data,
      user_id         : user.id,
      next_delivery   : data.start_date,
      no_of_days      : days,
      product         : compactProduct,
      amount          : total,
      base_amount     : total,
      delivery_address: user.address + "(near " + user.landmark + ")" + location.title + ", " + city.title + ", " + user.pincode,
      discount        : discount
    });
  }

  async delete(subscription: Subscription): Promise<any> {
    return subscription.destroy();
  }

  async addToOrder(): Promise<Order[]> {
    const today           = moment().format("YYYY-MM-DD");
    const subscriptions   = await Subscription.findAll({
      where     : {
        next_delivery: today
      }, include: [User]
    });
    const orders: Order[] = [];
    for (const subscription of subscriptions) {
      const cart = await Cart.findOne({
        where: {
          user_id: subscription.user_id
        }
      });

      const transaction = await dbService.getSequelize().transaction();
      try {
        const order = await Order.create({
          user_id         : subscription.user_id,
          cart_id         : cart.id,
          order_id        : 2 * Date.now(),
          products        : [subscription.product],
          delivery_address: subscription.delivery_address,
          delivery_code   : await Helpers.generateRandomString(6, {
            includeNumbers: true,
          }),
          base_amount     : subscription.base_amount / subscription.no_of_days,
          amount          : subscription.amount / subscription.no_of_days,
          discount        : subscription.discount / subscription.no_of_days,
          payment_mode    : PaymentModes.WALLET,
          payment_status  : PaymentStatus.SUCCESS,
          order_status    : OrderStatus.PENDING,
          order_type      : OrderType.SUBSCRIPTION,
          expected_date   : today
        }, {transaction});

        if (subscription.end_date != subscription.next_delivery) {
          let next_delivery: string = subscription.next_delivery.toString();
          if (subscription.subscription_type === SubscriptionTypes.DAILY) {
            next_delivery = moment(subscription.next_delivery).add(1, "days").format("YYYY-MM-DD");
          } else if (subscription.subscription_type === SubscriptionTypes.ALTERNATIVE) {
            next_delivery = moment(subscription.next_delivery).add(2, "days").format("YYYY-MM-DD");
          } else {
            next_delivery = moment(subscription.next_delivery).add(7, "days").format("YYYY-MM-DD");
          }

          await subscription.update({
            next_delivery: next_delivery
          }, {transaction});
        } else {
          await subscription.update({
            next_delivery: null
          }, {transaction});
        }

        orders.push(order);
        await transaction.commit();
      } catch (e) {
        await transaction.rollback();
        throw new UnprocessableEntityException(e);
      }
    }
    return orders;
  }

}

export const subscriptionService = SubscriptionService.getInstance();
