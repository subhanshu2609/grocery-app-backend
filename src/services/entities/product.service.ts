import logger from "../../util/logger.util";
import { ProductCategory } from "../../models/product-category.model";
import { ProductSubCategory } from "../../models/product-sub-category.model";
import { Product } from "../../models/product.model";
import { ENV_BASE_URL } from "../../util/secrets.util";
import { ProductCreateDto } from "../../dtos/product/product-create.dto";
import { ProductUpdateDto } from "../../dtos/product/product-update.dto";
import { ProductIndexDto } from "../../dtos/product/product-index.dto";
import { Transaction } from "sequelize";

class ProductService {
  constructor() {
    logger.silly("[N-FT] ProductService");
  }

  static getInstance(): ProductService {
    return new ProductService();
  }

  async showCategory(categoryId: number): Promise<ProductCategory> {
    return ProductCategory.findById(categoryId);
  }

  async showSubCategory(categoryId: number): Promise<ProductSubCategory> {
    return ProductSubCategory.findById(categoryId);
  }

  async showProduct(categoryId: number): Promise<Product> {
    return Product.findById(categoryId);
  }

  async showProductById(product_id: number): Promise<Product> {
    return Product.findById(product_id);
  }

  async listCategories(): Promise<ProductCategory[]> {
    return ProductCategory.findAll();
  }

  async listSubCategories(category_id: number): Promise<ProductSubCategory[]> {
    return ProductSubCategory.findAll({
      where: {
        category_id: category_id
      }
    });
  }

  async listProducts(sub_category_id: number, city_id: number): Promise<Product[]> {
    return Product.findAll({
      where: {
        sub_category_id: sub_category_id,
        city_id        : city_id
      },
      order: [["title", "asc"]]
    });
  }

  async allProducts(city_id?: number, filters?: ProductIndexDto, withIncludes?: boolean): Promise<Product[]> {
    if (filters.query && filters.query !== "") {
      return Product.findAll({
        where: {
          ["title" as any]: {
            like: "%" + filters.query + "%"
          },
          city_id         : city_id

        },
        order: [["title", "asc"]]
      });
    }
    return [];
  }

  async addCategory(title: string): Promise<ProductCategory> {
    return ProductCategory.create({
      title: title
    });
  }

  async addSubCategory(title: string, category_id: number, upload_url?: string): Promise<ProductSubCategory> {
    return ProductSubCategory.create({
      title      : title,
      category_id: category_id,
      image_url  : upload_url ? upload_url : ""
    });
  }

  async addProduct(data: ProductCreateDto, image_url?: string, transaction?: Transaction): Promise<Product> {
    return Product.create({
      ...data,
      image_url: image_url,
    }, {transaction});
  }

  async updateCategory(category: ProductCategory, title: string): Promise<ProductCategory> {
    return category.update({
      title: title
    });
  }

  async updateSubCategory(subCategory: ProductSubCategory, title: string, image_url?: string): Promise<ProductSubCategory> {
    return subCategory.update({
      title    : title,
      image_url: image_url
    });
  }

  async updateProduct(product: Product, data: ProductUpdateDto, image_url?: string): Promise<Product> {
    return product.update({
      ...data,
      image_url: image_url ? image_url : product.image_url,
    });
  }

  async deleteCategory(category: ProductCategory) {
    return category.destroy();
  }

  async deleteSubCategory(subCategory: ProductSubCategory) {
    return subCategory.destroy();
  }

  async deleteProduct(product: Product) {
    return product.destroy();
  }

}

export const productService = ProductService.getInstance();
