import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { Order } from "../../models/order.model";
import { OrderCreateDto } from "../../dtos/order/order-create.dto";
import moment from "moment";
import { Helpers } from "../../util/helpers.util";
import { PaymentStatus } from "../../enums/payment-status.enum";
import { OrderStatus } from "../../enums/order-status.enum";
import { OrderUpdateDto } from "../../dtos/order/order-update.dto";
import { cartService } from "./cart.service";
import { InvalidAmountException } from "../../exceptions/user/invalid-amount.exception";
import { User } from "../../models/user.model";
import { addressService } from "./address.service";
import { OrderType } from "../../enums/order-type.enum";
import { Employee } from "../../models/employee.model";

class OrderService {
  constructor() {
    logger.silly("[N-FT] OrderService");
  }

  static getInstance(): OrderService {
    return new OrderService();
  }

  async showOrder(order_id: number): Promise<Order> {
    return Order.findOne({
      where  : {
        id: order_id
      },
      include: [User]
    });
  }

  async showByEmployee(employee_id: number): Promise<Order[]> {
    return Order.findAll({
      where: {
        employee_id: employee_id
      }
    });
  }

  async showIncompleteOrders(user_id: number): Promise<Order[]> {
    return Order.findAll({
      where: {
        user_id: user_id
      },
      order: [["expected_date", "asc"]],
      include: [Employee]
    });
  }

  async showEnrouteOrders(user_id: number): Promise<Order[]> {
    return Order.findAll({
      where: {
        user_id: user_id,
        order_status: [OrderStatus.PENDING, OrderStatus.ON_THE_WAY, OrderStatus.ACCEPTED]
      },
      order: [["expected_date", "asc"]],
      include: [Employee]
    });
  }


  async addOrder(data: OrderCreateDto, user: User, amount: number, transaction?: Transaction): Promise<Order> {
    const cart = await cartService.showCart(data.cart_id);
    if (cart.amount !== amount) {
      throw new InvalidAmountException();
    }
    const city          = await addressService.findCity(user.city_id);
    const location      = await addressService.findLocation(user.location_id);
    const previousOrder = await Order.findOne({
      where: {
        user_id      : user.id,
        expected_date: moment().add(1, "days").format()
      }
    });
    const order         = {
      ...data,
      products        : cart.products,
      user_id         : user.id,
      delivery_address: user.address + "(near " + user.landmark + ")" + " , " + location.title + ", " + city.title + ", " + user.pincode,
      order_id        : 2 * Date.now(),
      base_amount     : cart.base_amount,
      amount          : cart.amount + cart.delivery_charge,
      delivery_charge : cart.delivery_charge,
      discount        : cart.discount,
      coupon_code     : cart.coupon_id,
      order_type      : OrderType.CART,
      delivery_code   : previousOrder ? previousOrder.delivery_code :
        await Helpers.generateRandomString(6, {
          includeUpperCase: false,
          includeLowerCase: false,
          includeNumbers  : true
        }),
      payment_status  : PaymentStatus.PENDING,
      order_status    : OrderStatus.PENDING,
      expected_date   : moment().add(1, "days").format("YYYY-MM-DD")
    };
    return Order.create(order);
  }

  async updateOrder(order: Order, data: OrderUpdateDto, transaction?: Transaction): Promise<Order> {
    return order.update(data, {transaction});
  }

  async deleteOrder(order: Order, transaction?: Transaction): Promise<any> {
    return order.destroy({transaction});
  }

}

export const orderService = OrderService.getInstance();
