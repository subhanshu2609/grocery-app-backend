import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { Cart } from "../../models/cart.model";
import { CartCreateDto } from "../../dtos/cart/cart-create.dto";
import { CartUpdateDto } from "../../dtos/cart/cart-update.dto";
import { productService } from "./product.service";
import { ProductNotFoundException } from "../../exceptions/product/product-not-found.exception";
import { Product } from "../../models/product.model";
import { Sequelize } from "sequelize";
import { User } from "../../models/user.model";
import { CompactProduct } from "../../models/compact-product.model";

class CartService {
  constructor() {
    logger.silly("[N-FT] CartService");
  }

  static getInstance(): CartService {
    return new CartService();
  }

  async showCart(cartId: number): Promise<Cart> {
    return Cart.findById(cartId);
  }

  async listCart(user_id: number): Promise<Cart> {
    return Cart.findOne({
      where     : {
        user_id: user_id
      }, include: [User]
    });
  }

  async createCart(user_id: number, transaction?: Transaction): Promise<Cart> {
    return Cart.create({
      user_id: user_id,
      amount : 0
    }, {transaction});
  }

  async updateProductOfCart(cart: Cart, data: CartCreateDto): Promise<Cart> {
    const index                    = cart.products.findIndex((p) => p.product_id === data.product_id);
    let newAmount                  = 0;
    let base_amount                = 0;
    let products: CompactProduct[] = [];
    if (index === -1) {
      const product = await productService.showProductById(data.product_id);
      if (!product) {
        throw new ProductNotFoundException();
      }
      let amount;
      if (product.selling_price && product.selling_price > 0) {
        amount = product.selling_price;
      } else {
        amount = product.mrp;
      }
      // const rate = product.is_under_gst ? amount * (100 + product.gst_rate) / 100 : amount;
      products = cart.products;
      await products.push({
        ...data,
        base_rate    : product.mrp,
        rate         : product.selling_price,
        gst_amount   : product.is_under_gst ? Math.ceil(amount * product.gst_rate / 10) / 10 : 0,
        base_quantity: product.base_quantity,
        title        : product.title,
        image_url    : product.image_url
      });

    } else {
      products                    = cart.products;
      products[index].no_of_units = data.no_of_units;
    }

    for (const cartProduct of products) {
      newAmount   = newAmount + (cartProduct.no_of_units * (cartProduct.rate + cartProduct.gst_amount));
      base_amount = base_amount + (cartProduct.base_rate * cartProduct.no_of_units);
    }
    let delivery_charge = 0;
    if (newAmount < 150) {
      delivery_charge = 15;
    }
    return cart.update({
      products       : products,
      base_amount    : base_amount,
      amount         : Math.round(newAmount * 10) / 10,
      delivery_charge: delivery_charge
    });
  }

  async removeProductFromCart(cart: Cart, product_id: number): Promise<any> {
    const products  = await cart.products.filter((product) => product.product_id !== product_id);
    let newAmount   = 0;
    let base_amount = 0;
    let delivery_charge = 0;
    for (const cartProduct of products) {
      newAmount   = newAmount + (cartProduct.no_of_units * (cartProduct.rate + cartProduct.gst_amount));
      base_amount = base_amount + (cartProduct.base_rate * cartProduct.no_of_units);
    }
    if (newAmount < 150) {
      delivery_charge = 15;
    }
    return cart.update({
      products   : products,
      base_amount: base_amount,
      amount     : newAmount,
      delivery_charge: delivery_charge
    });
  }

  async emptyCart(cart: Cart) {
    return cart.update({
      products   : [],
      base_amount: 0,
      amount     : 0,
      discount   : 0,
      coupon_code: null,
      coupon_id  : null
    });
  }

  async productsFromCart(cart: Cart, city_id: number): Promise<Product[]> {
    const products: Product[] = [];
    for (const product of cart.products) {
      const fullProduct = await productService.showProductById(product.product_id);
      const totalCount  = product.no_of_units;
      for (let i = 0; i < totalCount; i++) {
        products.push(fullProduct);
      }
      // for (const count of totalCount) {
      // }
    }
    return products;
  }

  async countQuantityOfProduct(user_id: number, product_id: number) {
    const cart = await this.listCart(user_id);
    let count  = 0;
    for (const product of cart.products) {
      if (product.product_id === product_id) {
        count = product.no_of_units;
      }
    }
    return count;
  }

  async resetCart(cart: Cart) {
    return cart.update({
      discount   : 0,
      coupon_id  : null,
      coupon_code: null
    });
  }
}

export const cartService = CartService.getInstance();
