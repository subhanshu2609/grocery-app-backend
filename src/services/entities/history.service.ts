import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { Order } from "../../models/order.model";
import { History } from "../../models/history.model";
import { HistoryCreateDto } from "../../dtos/order/history-create.dto";
import { User } from "../../models/user.model";
import { GlobalVariable } from "../../models/global-variable.model";
import { OrderStatus } from "../../enums/order-status.enum";
import moment from "moment";

class HistoryService {
  constructor() {
    logger.silly("[N-FT] HistoryService");
  }

  static getInstance(): HistoryService {
    return new HistoryService();
  }

  async showHistory(user_id: number): Promise<History[]> {
    return History.findAll({
      where: {
        user_id: user_id
      }
    });
  }

  async createHistory(data: Order, user: User, transaction?: Transaction): Promise<History> {
    let invoice = null;

    if (data.order_status === OrderStatus.DELIVERED) {
      const invoice_no = await GlobalVariable.findOne({
        where: {
          title: "Delivery Count"
        }
      });
      invoice          = await invoice_no.update({value: invoice_no.value + 1});
      invoice          = invoice.value;
    }

    const history: HistoryCreateDto = {
      order_id        : data.order_id,
      user_id         : data.user_id,
      products        : data.products,
      amount          : data.amount,
      base_amount     : data.base_amount,
      delivery_charge : data.delivery_charge,
      delivery_date   : moment().toDate(),
      discount        : data.discount,
      delivery_address: data.delivery_address,
      employee_id     : data.employee_id,
      payment_mode    : data.payment_mode,
      payment_status  : data.payment_status,
      order_status    : data.order_status,
      order_type      : data.order_type,
      transaction_id  : data.transaction_id,
      transacted_at   : data.transacted_at,
      invoice_no      : invoice
    };
    return History.create(history, {transaction});
  }

  async delivered(filters: { query?: string }): Promise<History[]> {
    if (filters.query && filters.query != "") {
      return History.findAll({
        where: {
          ["order_id" as string]: {
            like: "%" + filters.query + "%"
          },
          order_status          : OrderStatus.DELIVERED
        }
      });
    }
    return History.findAll({
      where   : {
        order_status: OrderStatus.DELIVERED
      }, order: [["createdAt", "desc"]]

    });
  }

  async rejected(): Promise<History[]> {
    return History.findAll({
      where: {
        order_status: OrderStatus.REJECTED
      }
    });
  }

  async cancelled(): Promise<History[]> {
    return History.findAll({
      where: {
        order_status: OrderStatus.CANCELLED
      }
    });
  }

}

export const historyService = HistoryService.getInstance();
