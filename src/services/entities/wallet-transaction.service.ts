import logger from "../../util/logger.util";
import { City } from "../../models/address/city.model";
import { Location } from "../../models/address/location.model";
import { Area } from "../../models/address/area.model";
import { AddressCreateDto } from "../../dtos/address/address-create.dto";
import { ENV_BASE_URL } from "../../util/secrets.util";
import { Transaction } from "sequelize";
import { AddressType } from "../../enums/address-type.enum";
import { AddressNotFoundException } from "../../exceptions/address/address-not-found.exception";
import { WalletTransactionCreateDto } from "../../dtos/wallet-transaction/wallet-transaction-create.dto";
import { WalletTransaction } from "../../models/wallet-transaction.model";

class WalletTransactionService {
  constructor() {
    logger.silly("[N-FT] WalletTransactionService");
  }

  static getInstance(): WalletTransactionService {
    return new WalletTransactionService();
  }

  async showByUser(userId: number): Promise<WalletTransaction[]> {
    return await WalletTransaction.findAll({
      where: {
        user_id: userId
      }
    });
  }

  async createTransaction(data: WalletTransactionCreateDto, userId: number, transaction?: Transaction): Promise<WalletTransaction> {
    return WalletTransaction.create({
      ...data,
      user_id: userId,
    }, {transaction});
  }
}

export const walletTransactionService = WalletTransactionService.getInstance();
