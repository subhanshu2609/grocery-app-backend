import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { Cart } from "../../models/cart.model";
import { CartCreateDto } from "../../dtos/cart/cart-create.dto";
import { productService } from "./product.service";
import { ProductNotFoundException } from "../../exceptions/product/product-not-found.exception";
import { CompactProduct } from "../../models/compact-product.model";
import { Coupon } from "../../models/coupon.model";
import { CouponCreateDto } from "../../dtos/coupon/coupon-create.dto";

class CouponService {
  constructor() {
    logger.silly("[N-FT] CouponService");
  }

  static getInstance(): CouponService {
    return new CouponService();
  }

  async showCoupon(couponId: number): Promise<Coupon> {
    return Coupon.findById(couponId);
  }

  async listCoupons(user_id: number): Promise<Coupon[]> {
    let coupon;
    if (user_id) {
      coupon = await Coupon.findAll({
        where: {
          user_id: user_id
        }
      });
    }

    const forAll = await Coupon.findAll({
      where: {
        user_id: null
      }
    });

    coupon.push(...forAll);
    return coupon;
  }

  async createCoupon(data: CouponCreateDto, created_by?: number, transaction?: Transaction): Promise<Coupon[]> {
    const coupons: Coupon[] = [];

    if (data.user_ids) {
      for (const user_id of data.user_ids) {
        const coupon = await Coupon.create({
          ...data,
          user_id   : user_id,
          created_by: created_by
        }, {transaction});
        coupons.push(coupon);
      }
      return coupons;
    } else {
      const coupon = await Coupon.create({
        ...data,
        created_by: created_by
      }, {transaction});
      coupons.push(coupon);
      return coupons;
    }
  }
}

export const couponService = CouponService.getInstance();
