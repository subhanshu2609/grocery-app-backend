import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { User } from "./user.model";
import { isNullOrUndefined } from "util";
import { CompactProduct } from "./compact-product.model";
import { Helpers } from "../util/helpers.util";
import { OrderStatus } from "../enums/order-status.enum";
import { SubscriptionTypes } from "../enums/subscription-types.enum";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "subscriptions"
})
export class Subscription extends Model<Subscription> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  user_id: number;

  @Column({
    type: DataType.JSON,
    set : function (this: Subscription, value: CompactProduct) {
      this.setDataValue("product", value ? JSON.stringify(value) : null);
    },
    get : function (this: Subscription) {
      const value = this.getDataValue("product");
      return value ? JSON.parse(value) : null;
    }
  })
  product: CompactProduct;

  @Column(DataType.DATEONLY)
  start_date: Date;

  @Column(DataType.DATEONLY)
  next_delivery: Date;

  @Column(DataType.DATEONLY)
  end_date: Date;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<SubscriptionTypes>(SubscriptionTypes)}))
  subscription_type: SubscriptionTypes;

  @Column(DataType.INTEGER)
  no_of_days: number;

  @Column(DataType.FLOAT)
  base_amount: number;

  @Column(DataType.FLOAT)
  amount: number;

  @Column(DataType.FLOAT)
  delivery_charge: number;

  @Column(DataType.STRING)
  delivery_address: string;

  @Column(DataType.FLOAT)
  discount?: number;

  @BelongsTo(() => User)
  user: User;

}
