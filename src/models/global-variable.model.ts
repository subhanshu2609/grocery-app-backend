import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "global_variables"
})
export class GlobalVariable extends Model<GlobalVariable> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.STRING)
  title: string;

  @Column(DataType.FLOAT)
  value: number;
}
