import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ProductSubCategory } from "./product-sub-category.model";
import { City } from "./address/city.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "products"
})
export class Product extends Model<Product> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.STRING)
  title: string;

  @Unique
  @Column(DataType.STRING)
  slug: string;

  @Column(DataType.STRING)
  image_url: string;

  @Column(DataType.STRING)
  base_quantity: string;

  @Column(DataType.BOOLEAN)
  is_under_gst: boolean;

  @Column(DataType.FLOAT)
  gst_rate?: number;

  @ForeignKey(() => ProductSubCategory)
  @Column(DataType.BIGINT)
  sub_category_id: number;

  @Column(DataType.FLOAT)
  mrp: number;

  @Column(DataType.FLOAT)
  selling_price: number;

  @ForeignKey(() => City)
  @Column(DataType.BIGINT)
  city_id: number;

  @Default(1)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @Default(1)
  @Column(DataType.BOOLEAN)
  in_stock: boolean;

  @Column(DataType.FLOAT)
  rating: number;

  @BelongsTo(() => ProductSubCategory)
  sub_category: ProductSubCategory;

  @BelongsTo(() => City)
  city: City;

  @BeforeCreate
  static addSlug(instance: Product) {
    instance.slug = Helpers.slugify(instance.title);
  }

  // @AfterDelete
  // static deleteImage(instance: Address) {
  //   fs.unlinkSync(instance.image_url);
  // }

}
