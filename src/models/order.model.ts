import {
  AfterDelete, AllowNull,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ProductCategory } from "./product-category.model";
import { ProductSubCategory } from "./product-sub-category.model";
import { City } from "./address/city.model";
import { Product } from "./product.model";
import { User } from "./user.model";
import { Employee } from "./employee.model";
import { isNullOrUndefined } from "util";
import { PaymentModes } from "../enums/payment-modes.enum";
import { PaymentStatus } from "../enums/payment-status.enum";
import { OrderStatus } from "../enums/order-status.enum";
import { Cart } from "./cart.model";
import { CompactProduct } from "./compact-product.model";
import { OrderType } from "../enums/order-type.enum";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "orders"
})
export class Order extends Model<Order> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.BIGINT)
  order_id: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  user_id: number;

  @ForeignKey(() => Cart)
  @Column(DataType.BIGINT)
  cart_id: number;

  @Column({
    type: DataType.JSON,
    set : function (this: Cart, value: number[]) {
      this.setDataValue("products", JSON.stringify(value || []));
    },
    get : function (this: Cart) {
      const value = this.getDataValue("products");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  products: CompactProduct[];

  @Column(DataType.FLOAT)
  base_amount: number;

  @Column(DataType.FLOAT)
  amount: number;

  @Column(DataType.FLOAT)
  delivery_charge: number;

  @Column(DataType.FLOAT)
  discount?: number;

  @Column(DataType.STRING)
  delivery_address: string;

  @ForeignKey(() => Employee)
  @Column(DataType.BIGINT)
  employee_id?: number;

  @Column(DataType.BIGINT)
  coupon_code?: number;

  @Column(DataType.STRING)
  delivery_code?: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<PaymentModes>(PaymentModes)}))
  payment_mode: PaymentModes;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<PaymentStatus>(PaymentStatus)}))
  payment_status: PaymentStatus;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<OrderStatus>(OrderStatus)}))
  order_status: OrderStatus;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<OrderType>(OrderType)}))
  order_type: OrderType;

  @Column(DataType.DATEONLY)
  expected_date?: Date;

  @Column(DataType.STRING)
  transaction_id?: string;

  @Column(DataType.DATE)
  transacted_at?: Date;

  @Column(DataType.STRING)
  slot_time?: string;

  @Column(DataType.INTEGER)
  delivery_time?: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Employee)
  employee: Employee;

}
