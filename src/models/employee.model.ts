import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { City } from "./address/city.model";
import { Location } from "./address/location.model";
import { Area } from "./address/area.model";
import { genSaltSync, hashSync } from "bcrypt";
import { EmployeeCategory } from "./employee-category.model";

@Table({
  timestamps: true,
  paranoid  : true,
  tableName : "employees"
})
export class Employee extends Model<Employee> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  name: string;

  @Unique
  @Column(DataType.STRING)
  email: string;

  @Unique
  @Column(DataType.STRING)
  mobile_no: string;

  @Column({
    type: DataType.STRING,
    set : function (this: Employee, value: string) {
      this.setDataValue("password", hashSync(value, genSaltSync(2)));
    }
  })
  password: string;

  @Unique
  @Column(DataType.STRING)
  aadhaar_no: string;

  @Unique
  @Column(DataType.STRING)
  driver_license: string;

  @ForeignKey(() => EmployeeCategory)
  @Column(DataType.BIGINT)
  category_id: number;

  @Column(DataType.STRING)
  state: string;

  @Column(DataType.STRING)
  city: string;

  @Column(DataType.STRING)
  location: string;

  @Column(DataType.STRING)
  area: string;

  @Column(DataType.STRING)
  pincode: string;

  @ForeignKey(() => City)
  @Column(DataType.BIGINT)
  city_id: number;

  @Column(DataType.BIGINT)
  createdBy: number;

  @BelongsTo(() => EmployeeCategory)
  category: EmployeeCategory;
}
