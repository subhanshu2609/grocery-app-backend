import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import * as fs from "fs";
import { Helpers } from "../util/helpers.util";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "attachments"
})
export class Attachment extends Model<Attachment> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.STRING)
  title: string;

  @Unique
  @Column(DataType.STRING)
  image_url: string;

  @Unique
  @Column(DataType.STRING)
  slug: string;

  @Default(0)
  @Column(DataType.INTEGER)
  priority: number;

  @BeforeCreate
  static addSlug(instance: Attachment) {
    instance.slug = Helpers.slugify(instance.title);
  }

  // @AfterDelete
  // static deleteImage(instance: Address) {
  //   fs.unlinkSync(instance.image_url);
  // }

}
