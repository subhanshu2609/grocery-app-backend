import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ProductSubCategory } from "./product-sub-category.model";
import { City } from "./address/city.model";
import { Product } from "./product.model";
import { User } from "./user.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "product_reviews"
})
export class ProductReview extends Model<ProductReview> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.INTEGER)
  rating: number;

  @Unique
  @Column(DataType.STRING)
  comment?: string;

  @ForeignKey(() => Product)
  @Column(DataType.BIGINT)
  product_id: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  user_id: number;

  @BelongsTo(() => Product)
  product: Product;

  @BelongsTo(() => User)
  user: User;
}
