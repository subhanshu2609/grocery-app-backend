import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { User } from "./user.model";
import { Helpers } from "../util/helpers.util";
import { SubscriptionTypes, UpdateWalletTypes } from "../enums/subscription-types.enum";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "wallet_transactions"
})
export class WalletTransaction extends Model<WalletTransaction> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  user_id: number;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<UpdateWalletTypes>(UpdateWalletTypes)}))
  type: UpdateWalletTypes;

  @Column(DataType.FLOAT)
  amount: number;

  @Column(DataType.STRING)
  about?: string;

  @BelongsTo(() => User)
  user: User;
}
