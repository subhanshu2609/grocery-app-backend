import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import * as fs from "fs";
import { Helpers } from "../util/helpers.util";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "slots"
})
export class Slot extends Model<Slot> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  start_time: string;

  @Column(DataType.STRING)
  end_time: string;

  @Column(DataType.BOOLEAN)
  is_active: boolean;
}
