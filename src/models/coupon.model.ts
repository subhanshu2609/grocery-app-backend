import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { User } from "./user.model";
import { isNullOrUndefined } from "util";
import { CompactProduct } from "./compact-product.model";
import { Employee } from "./employee.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "coupons"
})
export class Coupon extends Model<Coupon> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  user_id?: number;

  @Unique
  @Column(DataType.STRING)
  code: string;

  @Column(DataType.FLOAT)
  off_amount?: number;

  @Column(DataType.FLOAT)
  off_percentage?: number;

  @Column(DataType.FLOAT)
  minimum_cart_value?: number;

  @Column(DataType.BIGINT)
  frequency?: number;

  @ForeignKey(() => Employee)
  @Column(DataType.BIGINT)
  created_by: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Employee)
  employee: Employee;

}
